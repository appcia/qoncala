#ifndef CORE_BOARD_H
#define CORE_BOARD_H

#include <QtCore>
#include "core_bowl.h"

namespace core
{

class Game;

class Board
{
public:
    explicit Board(Game * game);
    virtual ~Board();

    Game * getGame() const;
    QVector<Bowl *> getBowls() const;
    QVector<Bowl *> getLine(short num);
    Bowl * getBowl(short num);
    Stone * getStone(short num);

    void addBowl(Bowl * bowl);

    void prepare();
    void reset();

private:
    Game * game;
    QVector<Bowl *> bowls;
    QVector<QVector<Bowl *> > lines;
};

}

#endif // CORE_BOARD_H
