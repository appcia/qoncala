#include "core_controller.h"
#include "core_exception.h"
#include "core_io.h"

namespace core {

Controller::Controller()
{
    gameWorker = NULL;
    game = NULL;
    io = new IO();
}

Controller::~Controller()
{
    delete io;
}

Controller & Controller::instance()
{
    static Controller singleton;

    return singleton;
}

Game *Controller::getGame() const
{
    return game;
}

bool Controller::isGameable() const
{
    return game != NULL;
}

void Controller::startGame(Game *game)
{
    if (game == NULL) {
        throw new Exception("Cannot start game which is null");
    }

    if (isGameable()) {
        endGame();
    }

    GameWorker * worker = new GameWorker(game);

    this->gameWorker = worker;
    this->game = game;

    game->setWorker(worker);
    worker->start();
}

void Controller::endGame()
{
    if (gameWorker != NULL) {
        gameWorker->terminate();
        gameWorker->wait();

        delete gameWorker;
        gameWorker = NULL;
    }

    if (game != NULL) {
        game->clean();

        delete game;
        game = NULL;
    }
}

IO *Controller::getIO()
{
    return io;
}

} // namespace core
