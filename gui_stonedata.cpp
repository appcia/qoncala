#include "gui_stonedata.h"
#include "core_exception.h"

namespace gui
{

StoneData::StoneData()
{
    format << "stone" << "object/stone";
}

void StoneData::setStone(core::Stone * stone)
{
    if (stone == NULL) {
        throw new core::Exception("Stone to be stored cannot be null");
    }

    this->stone = stone;
}

core::Stone * StoneData::getStone() const
{
    return this->stone;
}

bool StoneData::hasFormat(const QString &mimetype) const
{
    if (mimetype == "stone") {
        return true;
    }

    return false;
}

QStringList StoneData::formats()
{
    return this->format;
}

}
