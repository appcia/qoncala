#ifndef CORE_GAMESTATE_H
#define CORE_GAMESTATE_H

#include <QtCore>

namespace core
{

class Game;

class GameState {
public:
    GameState(Game * game);
    virtual ~GameState();

    Game * getGame();
    void addChild(GameState * child);
    bool hasChildren();
    void addChildren(QVector<GameState *> children);
    QVector<GameState *> & getChildren();

private:
    Game * game;
    QVector<GameState *> children;
};

}

#endif // CORE_GAMESTATE_H
