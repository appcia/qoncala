#include "gui_helpdialog.h"
#include "ui_gui_helpdialog.h"

#include <QFile>
#include <QMessageBox>
#include <QDebug>
#include <QDir>

namespace gui {

HelpDialog::HelpDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HelpDialog)
{
    ui->setupUi(this);

    pages[0] = "gameRules.html";
    pages[1] = "authors.html";
    pages[2] = "license.html";

    connect(ui->pageList, SIGNAL(currentRowChanged(int)), this, SLOT(loadPage(int)));
}

HelpDialog::~HelpDialog()
{
    delete ui;
}

void HelpDialog::loadPage(int index)
{
    ui->pageBrowser->setSource(QUrl::fromLocalFile(":/resource/help/" + pages[index]));
}

} // namespace gui
