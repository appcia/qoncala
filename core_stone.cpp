#include "core_stone.h"
#include "core_bowl.h"
#include "core_exception.h"

namespace core
{

Stone::Stone(Bowl * bowl, Stone::Color color, short num)
{
    locked = false;

    if (bowl == NULL) {
        throw new Exception("Bowl cannot be null");
    }
    bowl->addStone(this);

    this->color = color;
    this->num = num;

    this->bowl = bowl;
    bowl->addStone(this);
}

Stone::Color Stone::getColor()
{
    return color;
}

QString Stone::getColorName() {
    switch (color) {
    case C_White:
        return "white";
    case C_Black:
        return "black";
    case C_Green:
        return "green";
    case C_Red:
        return "red";
    default:
        return "unknown";
    }
}

Bowl * Stone::getBowl()
{
    return bowl;
}

void Stone::setBowl(Bowl * bowl)
{
    if (bowl == NULL) {
        throw new Exception("Bowl cannot be null");
    }

    this->bowl = bowl;
}

void Stone::setLocked(bool flag)
{
    locked = flag;
}

bool Stone::isLocked()
{
    return locked;
}

short Stone::getNum() const
{
    return num;
}

}
