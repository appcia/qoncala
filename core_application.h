#ifndef CORE_APPLICATION_H
#define CORE_APPLICATION_H

#include <QApplication>

namespace core {

class Application : public QApplication
{
    Q_OBJECT
public:
    explicit Application(int &argc, char *argv[]);
    bool notify(QObject *, QEvent *);
    
signals:
    
public slots:
    
};

} // namespace core

#endif // CORE_APPLICATION_H
