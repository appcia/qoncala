#ifndef CORE_ACTION_MOVESTONE_H
#define CORE_ACTION_MOVESTONE_H

#include "core_action.h"
#include "core_bowl.h"

namespace core
{

namespace action
{

class MoveStone : public Action
{
public:
    MoveStone(short bowlFrom, short bowlTo, short stone);

    void execute();
    void undo();
    bool isLegal();

    short getBowlTo() const;
    short getBowlFrom() const;
    short getStone() const;

private:
    short bowlFrom;
    short bowlTo;
    short stone;
};

}

}

#endif // CORE_ACTION_MOVESTONE_H
