#ifndef CORE_SELECTBOWL_H
#define CORE_SELECTBOWL_H

#include "core_action.h"
#include "core_bowl.h"

namespace core
{

namespace action
{

class SelectBowl : public Action
{
public:
    SelectBowl(short bowl);

    void execute();
    void undo();
    bool isLegal();

    short getBowl() const;
private:
    short bowl;
};

}

}

#endif // CORE_SELECTBOWL_H
