#ifndef CORE_GAMEWORKER_H
#define CORE_GAMEWORKER_H

#include "core_condition.h"

#include <QThread>
#include <QDebug>

namespace core
{
class Game;

class GameWorker : public QThread
{
    Q_OBJECT
public:
    static const int AIDelay = 2000;
    static const int Delay = 50;

    explicit GameWorker(Game * game);

    bool isWaiting() const;
    void setWaiting(bool flag);

    Condition * getCondition() const;

signals:
    void aiProcessingStarted();
    void aiProcessingEnded();

    void conditionWaiting();
    void actionExecuted();
    void conditionDone();

protected:
    void run();
    void waitForCondition(Condition * condition);

private:
    Game * game;
    bool waiting;
    Condition * condition;
};

}

#endif // CORE_GAMEWORKER_H
