#include "core_player.h"
#include "core_game.h"
#include "core_exception.h"
#include "core_bowl.h"

namespace core
{

Player::Player(Game * game, Stone::Color stoneColor)
{
    if (game == NULL) {
        throw new Exception("Game cannot be null");
    }

    this->game = game;
    this->stoneColor = stoneColor;

    type = T_Human;
    ai = false;
    scoreBowl = new Bowl();
    selectedBowl = NULL;

    game->setPlayer(this);
}

Player::~Player()
{
    if (scoreBowl) {
        delete scoreBowl;
        scoreBowl = NULL;
    }
}

Game *Player::getGame() const
{
    return game;
}

Stone::Color Player::getStoneColor() const
{
    return stoneColor;
}

Bowl * Player::getSelectedBowl() const
{
    return selectedBowl;
}

bool Player::hasSelectedBowl() const
{
    return selectedBowl != NULL;
}

void Player::selectBowl(Bowl * bowl)
{
    if (bowl == NULL) {
        throw new Exception("Cannot select bowl which is null");
    }

    deselectBowl();

    selectedBowl = bowl;
    selectedBowl->setSelected(true);
}

void Player::deselectBowl()
{
    if (selectedBowl == NULL) {
        return;
    }

    selectedBowl->setSelected(false);
    selectedBowl = NULL;
}

QVector<Stone *> Player::getAffectedStones() const
{
    return affectedStones;
}

Bowl *Player::getScoreBowl() const
{
    return scoreBowl;
}

int Player::getScore() const
{
    return scoreBowl->getStoneCount();
}

bool Player::isAI() const
{
    return ai;
}

QString Player::getName() const
{
    switch (stoneColor) {
    case Stone::C_Black:
        return "Player 1 (black)";
        break;
    case Stone::C_White:
        return "Player 2 (white)";
        break;
    default:
        return "Player X (unknown)";
        break;
    }
}

Player::Type Player::getType() const
{
    return type;
}

QString Player::getTypeName() const
{
    switch (type) {
    case T_AI:
        return "AI";
    case T_Human:
        return "Human";
    default:
        return "Unknown";
    }
}

void Player::addAffectedStone(Stone * stone)
{
    if (stone == NULL) {
        throw new Exception("Affected stone cannot be null");
    }

    affectedStones.push_back(stone);
    stone->setLocked(true);
}

void Player::removeAffectedStone(Stone * stone)
{
    if (stone == NULL) {
        throw new Exception("Affected stone cannot be null");
    }

    affectedStones.erase(std::remove(affectedStones.begin(), affectedStones.end(), stone), affectedStones.end());
    stone->setLocked(false);
}

void Player::clearAffectedStones()
{
    foreach (Stone * stone, affectedStones) {
        stone->setLocked(false);
    }

    affectedStones.clear();
}

}
