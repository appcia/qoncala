#include "core_action_clearboard.h"
#include "core_game.h"
#include "core_player.h"

namespace core {
namespace action {

ClearBoard::ClearBoard()
{
    name = N_ClearBoard;
    selectedBowl = -1;
}

void ClearBoard::execute()
{
    Player * p = game->getCurrentPlayer();

    foreach (Stone * stone, p->getAffectedStones()) {
        affectedStones.push_back(stone->getNum());
    }
    selectedBowl = game->getCurrentPlayer()->getSelectedBowl()->getNum();

    p->clearAffectedStones();
    p->deselectBowl();
}

void ClearBoard::undo()
{
    Player * p = game->getCurrentPlayer();

    foreach (int stone, affectedStones) {
        p->addAffectedStone(game->getBoard()->getStone(stone));
    }

    p->selectBowl(game->getBoard()->getBowl(selectedBowl));
}

bool ClearBoard::isLegal()
{
    return (game->getPreviousAction()->getName() == Action::N_MoveStone);
}

} // namespace action
} // namespace core
