#include "core_bowl.h"
#include "core_board.h"
#include "core_exception.h"

namespace core
{

Bowl::Bowl(Board * board, short num)
{
    selected = false;
    highlighted = false;
    neighbours.resize(Position_Count);

    if (board == NULL) {
        throw new Exception("Board cannot be null");
    }

    this->board = board;
    board->addBowl(this);

    this->num = num;
}

bool Bowl::isEqual(Bowl * bowl) const
{
    QVector<Stone::Color> _stones = bowl->getStonesByColor();

    if (_stones.count() != stones.count()) {
        return false;
    }

    foreach (Stone * stone, stones) {
        Stone::Color color = stone->getColor();
        if (_stones.contains(color)) {
            _stones.remove(_stones.indexOf(color));
        } else {
            return false;
        }
    }

    return true;
}

Bowl::Bowl()
{
    board = NULL;
    num = Custom;
    selected = false;
    highlighted = false;
}

Bowl::~Bowl()
{
    foreach (Stone * stone, stones) {
        delete stone;
    }

    stones.clear();
    neighbours.clear();
}

void Bowl::setBoard(Board *board)
{
    this->board = board;
}

Board *Bowl::getBoard() const
{
    return board;
}

short Bowl::getNum()
{
    return num;
}

bool Bowl::isCustom()
{
    return num == Custom;
}

void Bowl::setNeighbour(Bowl *bowl, Position position)
{
    short i = (short) position;
    neighbours[i] = bowl;
}

QVector<Bowl *> Bowl::getNeighbours(bool onlyExisting) const
{
    if (onlyExisting) {
        QVector<Bowl *> existing;
        foreach (Bowl * bowl, neighbours) {
            if (bowl) {
                existing.push_back(bowl);
            }
        }
        return existing;
    }

    return neighbours;
}

Bowl * Bowl::getNeighbour(Position position) const
{
    return neighbours[position];
}

Bowl *Bowl::getNeighbour(short position) const
{
    if (position < 0 || position >= Position_Count) {
        throw new Exception("Invalid neighbour position");
    }

    return neighbours[position];
}

bool Bowl::hasStone(Stone::Color color)
{
    foreach (Stone * stone, stones) {
        if (stone->getColor() == color) {
            return true;
        }
    }

    return false;
}

Stone *Bowl::getStone(short num)
{
    if (num < 1 || num > 24) {
        throw new Exception("Stone num out of range");
    }

    foreach (Stone * stone, stones) {
        if (stone->getNum() == num) {
            return stone;
        }
    }

    throw new Exception("Stone by num: " + QString::number(num) + " not found in bowl: " + this->num);
}

Stone * Bowl::getStone(Stone::Color color)
{
    foreach (Stone * stone, stones) {
        if (stone->getColor() == color) {
            return stone;
        }
    }

    return NULL;
}

void Bowl::addStone(Stone * stone) {
    if (stone == NULL) {
        throw new Exception("Stone to be added cannot be null");
    }

    if (stones.contains(stone)) {
        return;
    }

    if (stones.size() < Capacity) {
        stones.push_back(stone);
        stone->setBowl(this);
    }
}

void Bowl::removeStone(Stone * stone){
    if (stone == NULL) {
        throw new Exception("Stone to be removed cannot be null");
    }

    stones.erase(std::remove(stones.begin(), stones.end(), stone), stones.end());
}

void Bowl::clearStones()
{
    stones.clear();
}

void Bowl::setSelected(bool selected)
{
    this->selected = selected;
}

bool Bowl::isSelected() const
{
    return selected;
}

QVector<Stone::Color> Bowl::getStonesByColor() const
{
    QVector<Stone::Color> stonesColor;
    foreach (Stone * stone, stones) {
        stonesColor.push_back(stone->getColor());
    }
    return stonesColor;
}

QVector<Stone *> Bowl::getStones(bool onlyUnaffected) const
{
    if (onlyUnaffected) {
        QVector<Stone *> unaffected;
        QVector<Stone::Color> unaffectedColors;
        foreach (Stone * stone, stones) {
            if (!stone->isLocked() && !unaffectedColors.contains(stone->getColor())) {
                unaffected.push_back(stone);
                unaffectedColors.push_back(stone->getColor());
            }
        }
        return unaffected;
    }

    return stones;
}

short Bowl::getStoneCount() const
{
    return stones.size();
}

bool Bowl::isFull()
{
    return (stones.size() == Capacity);
}

bool Bowl::isHighlighted() const
{
    return highlighted;
}

void Bowl::setHighlighted(bool flag)
{
    highlighted = flag;
}

}
