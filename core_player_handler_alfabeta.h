#ifndef CORE_PLAYER_HANDLER_ALFABETA_H
#define CORE_PLAYER_HANDLER_ALFABETA_H

#include "core_player_handler.h"

namespace core {
namespace player {
namespace handler {

class AlfaBeta : public Handler
{
public:
    AlfaBeta(int maxDepth);

    void createTree(GameState * node, int depth);
    QPair<Game *, int> prune(GameState * node, int depth, QPair<Game *, int> alpha, QPair<Game *, int> beta);

    core::Game * findBestGame();
protected:
    int maxDepth;
    Player::No maxPlayer;
};

} // namespace handler
} // namespace player
} // namespace core

#endif // CORE_PLAYER_HANDLER_ALFABETA_H
