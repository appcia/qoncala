#include "core_interface.h"
#include "core_game.h"
#include "core_exception.h"

namespace core {

Interface::Interface(Game * game)
{
    visible = true;

    if (game == NULL) {
        throw new Exception("Game cannot be null");
    }

    this->game = game;
    game->setInterface(this);
}

Game *Interface::getGame() const
{
    return game;
}

bool Interface::isVisible() const
{
    return visible;
}

void Interface::setVisible(bool visible)
{
    this->visible = visible;
}

} // namespace core
