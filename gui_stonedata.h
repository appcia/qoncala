#ifndef GUI_STONEDATA_H
#define GUI_STONEDATA_H

#include <QMimeData>
#include <QStringList>
#include "core_stone.h"

namespace gui
{
class StoneData : public QMimeData
{
    Q_OBJECT
public:
    StoneData();

    bool hasFormat(const QString &mimetype) const;
    QStringList formats();
    void setStone(core::Stone * stone);
    core::Stone * getStone() const;
private:
    core::Stone * stone;
    QStringList format;
};
}

#endif // GUI_STONEDATA_H
