#include "core_board.h"
#include "core_exception.h"
#include "core_game.h"

namespace core
{

Board::Board(Game * game)
{
    if (game == NULL) {
        throw new Exception("Game is null");
    }

    this->game = game;
    game->setBoard(this);

    Bowl * b1 = new Bowl(this, 1);
    Bowl * b2 = new Bowl(this, 2);
    Bowl * b3 = new Bowl(this, 3);
    Bowl * b4 = new Bowl(this, 4);
    Bowl * b5 = new Bowl(this, 5);
    Bowl * b6 = new Bowl(this, 6);
    Bowl * b7 = new Bowl(this, 7);
    Bowl * b8 = new Bowl(this, 8);
    Bowl * b9 = new Bowl(this, 9);
    Bowl * b10 = new Bowl(this, 10);
    Bowl * b11 = new Bowl(this, 11);
    Bowl * b12 = new Bowl(this, 12);

    b1->setNeighbour(b12, Bowl::P_LowerLeft);
    b1->setNeighbour(b2, Bowl::P_LowerRight);

    b2->setNeighbour(b1, Bowl::P_UpperLeft);
    b2->setNeighbour(b3, Bowl::P_Right);
    b2->setNeighbour(b4, Bowl::P_LowerRight);
    b2->setNeighbour(b12, Bowl::P_Left);

    b3->setNeighbour(b2, Bowl::P_Left);
    b3->setNeighbour(b4, Bowl::P_LowerLeft);

    b4->setNeighbour(b2, Bowl::P_UpperLeft);
    b4->setNeighbour(b3, Bowl::P_UpperRight);
    b4->setNeighbour(b5, Bowl::P_LowerRight);
    b4->setNeighbour(b6, Bowl::P_LowerLeft);

    b5->setNeighbour(b4, Bowl::P_UpperLeft);
    b5->setNeighbour(b6, Bowl::P_Left);

    b6->setNeighbour(b4, Bowl::P_UpperRight);
    b6->setNeighbour(b5, Bowl::P_Right);
    b6->setNeighbour(b7, Bowl::P_LowerLeft);
    b6->setNeighbour(b8, Bowl::P_Left);

    b7->setNeighbour(b6, Bowl::P_UpperRight);
    b7->setNeighbour(b8, Bowl::P_UpperLeft);

    b8->setNeighbour(b6, Bowl::P_Right);
    b8->setNeighbour(b7, Bowl::P_LowerRight);
    b8->setNeighbour(b9, Bowl::P_Left);
    b8->setNeighbour(b10, Bowl::P_UpperLeft);

    b9->setNeighbour(b10, Bowl::P_UpperRight);
    b9->setNeighbour(b8, Bowl::P_Right);

    b10->setNeighbour(b11, Bowl::P_UpperLeft);
    b10->setNeighbour(b12, Bowl::P_UpperRight);
    b10->setNeighbour(b8, Bowl::P_LowerRight);
    b10->setNeighbour(b9, Bowl::P_LowerLeft);

    b11->setNeighbour(b12, Bowl::P_Right);
    b11->setNeighbour(b10, Bowl::P_LowerRight);

    b12->setNeighbour(b1, Bowl::P_UpperRight);
    b12->setNeighbour(b10, Bowl::P_LowerLeft);
    b12->setNeighbour(b11, Bowl::P_Left);
    b12->setNeighbour(b2, Bowl::P_Right);

    QVector<core::Bowl *> line0;
    QVector<core::Bowl *> line1;
    QVector<core::Bowl *> line2;
    QVector<core::Bowl *> line3;
    QVector<core::Bowl *> line4;
    QVector<core::Bowl *> line5;

    line0.push_back(b11);
    line0.push_back(b12);
    line0.push_back(b2);
    line0.push_back(b3);
    lines.push_back(line0);

    line1.push_back(b1);
    line1.push_back(b2);
    line1.push_back(b4);
    line1.push_back(b5);
    lines.push_back(line1);

    line2.push_back(b1);
    line2.push_back(b12);
    line2.push_back(b10);
    line2.push_back(b9);
    lines.push_back(line2);

    line3.push_back(b9);
    line3.push_back(b8);
    line3.push_back(b6);
    line3.push_back(b5);
    lines.push_back(line3);

    line4.push_back(b7);
    line4.push_back(b6);
    line4.push_back(b4);
    line4.push_back(b3);
    lines.push_back(line4);

    line5.push_back(b7);
    line5.push_back(b8);
    line5.push_back(b10);
    line5.push_back(b11);
    lines.push_back(line5);
}

Board::~Board()
{
    foreach (Bowl * bowl, bowls) {
        delete bowl;
    }
}

Game *Board::getGame() const
{
    return game;
}

QVector<Bowl *> Board::getBowls() const
{
    return bowls;
}

Bowl *Board::getBowl(short num)
{
    if (num < 1 || num > 12) {
        throw new Exception("Bowl num out of range");
    }

    return bowls[num - 1];
}

QVector<Bowl *> Board::getLine(short num)
{
    if (num < 0 || num > 5) {
        throw new Exception("Invalid line number: " + QString::number(num));
    }

    return lines[num];
}

Stone *Board::getStone(short num)
{
    if (num < 1 || num > 24) {
        throw new Exception("Stone num out of range");
    }

    foreach (Bowl * bowl, bowls) {
        foreach (Stone * stone, bowl->getStones()) {
            if (stone->getNum() == num) {
                return stone;
            }
        }
    }

    throw new Exception("Stone not found in all bowls");
}

void Board::addBowl(Bowl *bowl)
{
    if (bowl == NULL) {
        throw new Exception("Bowl cannot be null");
    }

    if (bowls.contains(bowl)) {
        return;
    }

    bowl->setBoard(this);
    bowls.push_back(bowl);
}

void Board::prepare()
{
    reset();

    short n = 1;
    for (short w = 0; w < 9; w += 4) {
        new Stone(bowls[w], Stone::C_White, n++);
        new Stone(bowls[w], Stone::C_Green, n++);
        new Stone(bowls[w], Stone::C_Red, n++);
        new Stone(bowls[w], Stone::C_Yellow, n++);
    }

    for (short b = 2; b < 11; b += 4) {
        new Stone(bowls[b], Stone::C_Black, n++);
        new Stone(bowls[b], Stone::C_Green, n++);
        new Stone(bowls[b], Stone::C_Red, n++);
        new Stone(bowls[b], Stone::C_Yellow, n++);
    }
}

void Board::reset()
{
    foreach (Bowl * bowl, bowls) {
        bowl->setSelected(false);
        bowl->setHighlighted(false);
        bowl->clearStones();

    }
}

}
