#include "core_gamestate.h"
#include "core_game.h"

namespace core
{

GameState::GameState(Game * game)
{
    this->game = game;
}

GameState::~GameState()
{
    foreach (GameState * child, children) {
        delete child;
    }
}

Game * GameState::getGame()
{
    return game;
}

void GameState::addChild(GameState * child)
{
    children.push_back(child);
}

void GameState::addChildren(QVector<GameState *> children)
{
    this->children.clear();
    this->children = children;
}

bool GameState::hasChildren()
{
    return children.isEmpty();
}

QVector<GameState *> & GameState::getChildren()
{
    return children;
}

}
