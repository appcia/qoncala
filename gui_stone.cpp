#include "gui_stone.h"
#include "gui_bowl.h"
#include "gui_stonedata.h"
#include "core_game.h"
#include "core_action_capturestone.h"
#include "core_action_movestone.h"

namespace gui {

Stone::Stone(core::Stone * model, QGraphicsObject *parent) :
    View(parent)
{
    name = "Stone";

    this->model = model;

    setFlags(QGraphicsItem::ItemIsSelectable);
    setAcceptsHoverEvents(true);

    if (model->getBowl()->isCustom() || model->getBowl()->getBoard()->getGame()->getCurrentPlayer()->isAI()) {
        setEnabled(false);
    }
}

QRectF Stone::boundingRect() const
{
    return QRect(QPoint(-Size / 2, -Size / 2), QPoint(Size / 2, Size / 2));
}

void Stone::paint(QPainter * p, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QColor color;

    switch (model->getColor()) {
    case core::Stone::C_Black:
        color.setNamedColor("#000000");
        break;
    case core::Stone::C_White:
        color.setNamedColor("#ffffff");
        break;
    case core::Stone::C_Green:
        color.setNamedColor("#009900");
        break;
    case core::Stone::C_Red:
        color.setNamedColor("#CC0000");
        break;
    case core::Stone::C_Yellow:
        color.setNamedColor("#FFFF00");
        break;
    }

    p->setBrush(color);
    p->drawEllipse(QPoint(0, 0), Size / 2, Size / 2);
}

void Stone::generatePossibleMoves()
{
    const core::Bowl * bowl = model->getBowl();
    const core::Game * game = bowl->getBoard()->getGame();

    core::action::MoveStone * previousMove = static_cast<core::action::MoveStone *> (game->getPreviousAction());
    QVector<core::Bowl *> neighbours;

    if (previousMove->getName() == core::Action::N_SelectBowl) {
        neighbours = model->getBowl()->getNeighbours();
    } else if (previousMove->getName() == core::Action::N_MoveStone) {
        core::Bowl * to = game->getBoard()->getBowl(previousMove->getBowlTo());
        neighbours = to->getNeighbours();
    }

    this->possibleMoves = neighbours;
}

void Stone::highlightBowls(bool flag)
{
    foreach (core::Bowl * bowl, this->possibleMoves) {
        if (bowl && (bowl->isHighlighted() != flag)) {
            bowl->setHighlighted(flag);
        }
    }
}

void Stone::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);

    if (model->isLocked() || !model->getBowl()->isSelected()) {
        return;
    }

    setCursor(Qt::OpenHandCursor);
}

void Stone::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);

    if (model->isLocked()) {
        return;
    }

    core::Bowl * bowl = model->getBowl();
    core::Game * game = bowl->getBoard()->getGame();

    if (bowl->isSelected()){
        setCursor(Qt::ClosedHandCursor);
        generatePossibleMoves();
    }

    if (game->getState() == core::Game::S_CAPTURE) {
        core::action::CaptureStone * action = new core::action::CaptureStone(bowl->getNum(), model->getNum());
        game->resolveCurrentAction(action);
    }
}

void Stone::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (!model->getBowl()->isSelected()) {
        return;
    }

    if (model->isLocked()) {
        return;
    }

    highlightBowls(true);
    scene()->update();

    QDrag * drag = new QDrag(event->widget());
    StoneData * data = new StoneData();

    data->setStone(model);
    drag->setMimeData(data);

    QPixmap pixmap(34, 34);
    pixmap.fill(Qt::white);

    QPainter painter(&pixmap);
    painter.translate(15, 15);
    painter.setRenderHint(QPainter::Antialiasing);
    paint(&painter, 0, 0);
    painter.end();

    pixmap.setMask(pixmap.createHeuristicMask());

    drag->setPixmap(pixmap);
    drag->setHotSpot(QPoint(15, 20));
    drag->exec();

    highlightBowls(false);
    scene()->update();
}

} // namespace gui
