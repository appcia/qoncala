#ifndef CORE_PLAYER_HANDLER_H
#define CORE_PLAYER_HANDLER_H

#include "core_controller.h"
#include "core_gamestate.h"

namespace core {

class Game;
class Action;

namespace player {

class AI;

class Handler
{
public:
    enum Type {
        T_Random,
        T_Heuristic,
        T_AlfaBeta,
        T_AlfaBetaHT
    };

    Handler();
    virtual ~Handler();

    virtual Game * findBestGame() = 0;

    QVector<Action *> getPlan(Game * node);

    Type getType() const;
    QString getTypeName() const;

    static int evaluateGame(Game * game);

protected:
    Type type;
    AI * player;
    GameState * rootState;

    void generateChildren(GameState * node, QVector<Action *> actions);
    void generateNextGames(GameState * node, QVector<Game *> & possibleStates, QHash<int, Game *> & distinctStates);

    void openRootState();
    void deleteRootState();
    void closeGameState(Game * game);
    bool isDuplicate(Game * game, QHash<int, Game *> distinctGames);

    QVector<Action *> getPossibleSelections(Game * game);
    QVector<Action *> getPossibleMoves(Game * game);
    QVector<Action *> getPossibleCaptures(Game * game);

    QVector<Game *> getAllPossibleGames(GameState * gameState);
};

} // namespace player
} // namespace core

#endif // CORE_PLAYER_HANDLER_H
