#ifndef CORE_ACTION_CAPTURESTONE_H
#define CORE_ACTION_CAPTURESTONE_H

#include "core_action.h"
#include "core_bowl.h"

namespace core
{

namespace action
{

class CaptureStone : public Action
{
public:
    CaptureStone(short bowlFrom, short stone);

    short getBowlFrom() const;
    short getStone() const;

    void execute();
    void undo();
    bool isLegal();
private:
    short bowlFrom;
    short stone;
};

}

}

#endif // CORE_ACTION_CAPTURESTONE_H
