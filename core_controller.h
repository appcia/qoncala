#ifndef CORE_CONTROLLER_H
#define CORE_CONTROLLER_H

#include "core_game.h"

namespace core {

class IO;

class Controller
{
public:
    static Controller & instance();

    bool isGameable() const;
    Game * getGame() const;

    void startGame(Game * game);
    void endGame();

    IO * getIO();

private:
    Controller();
    ~Controller();

    Game * game;
    GameWorker * gameWorker;
    IO * io;
};

} // namespace core

#endif // CORE_CONTROLLER_H
