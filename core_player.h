#ifndef CORE_PLAYER_H
#define CORE_PLAYER_H

#include "core_stone.h"
#include <QtCore>

namespace core
{

class Action;
class Bowl;
class Game;

class Player
{
public:
    enum Type {
        T_Human,
        T_AI
    };

    static const int No_Count = 2;
    enum No {
        N_First,
        N_Second
    };

    Player(Game * game, Stone::Color stoneColor);
    virtual ~Player();

    Game * getGame() const;

    QVector<Stone *> getCollectedStones() const;
    Stone::Color getStoneColor() const;

    Bowl * getSelectedBowl() const;
    bool hasSelectedBowl() const;
    void selectBowl(Bowl * selectedBowl);
    void deselectBowl();

    QVector<Stone *> getAffectedStones() const;
    void addAffectedStone(Stone * stone);
    void clearAffectedStones();
    void removeAffectedStone(Stone * stone);

    Bowl * getScoreBowl() const;
    int getScore() const;

    QString getName() const;
    Type getType() const;
    QString getTypeName() const;
    bool isAI() const;

protected:
    Game * game;

    Type type;
    bool ai;
    Stone::Color stoneColor;

    QVector<Stone *> affectedStones;
    Bowl * scoreBowl;
    Bowl * selectedBowl;
};

}

#endif // CORE_PLAYER_H
