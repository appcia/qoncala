#include "gui_button.h"
#include "core_exception.h"
#include "core_game.h"

namespace gui {

Button::Button(core::Interface *model, QGraphicsObject *parent) :
    View(parent)
{
    name = "Button";

    this->model = model;
}

void Button::setText(QString text)
{
    this->text = text;
}

QString Button::getText() const
{
    return text;
}

void Button::setSize(QRectF size)
{
    this->size = size;
}

QRectF Button::getSize() const
{
    return size;
}

QRectF Button::boundingRect() const
{
    return size;
}

void Button::paint(QPainter *p, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    const int scale = size.width() / 10;
    const bool enabled = model->getGame()->getState() == core::Game::S_CAPTURE;

    setCursor(enabled ? Qt::PointingHandCursor : Qt::ArrowCursor);

    p->setPen(QPen(QColor("#FFC162"), 3));
    p->setBrush(QColor(enabled ? "#FFC162" : "#D19E4F"));
    p->drawRoundedRect(size, 20, 20);

    p->setPen(QPen(QColor("#000000"), 1));
    p->setFont(QFont("Verdana", scale));
    p->drawText(size, text,  QTextOption(Qt::AlignCenter));
}

void Button::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);

    emit pressed();
}

} // namespace gui
