#include "core_player_handler_heuristic.h"


namespace core {
namespace player {
namespace handler {

Heuristic::Heuristic()
{
    type = T_Heuristic;
}

Game * Heuristic::findBestGame()
{
    openRootState();

    int bestVal = -100;
    int val = 0;
    Game * bestGame = NULL;

    QVector<Game *> possibleGames = getAllPossibleGames(rootState);

    foreach (Game * game, possibleGames) {
        val = evaluateGame(game);

        if (val > bestVal) {
            bestVal = val;
            bestGame = game;
        }
    }

    bestGame = bestGame->copy();
    closeGameState(bestGame);

    deleteRootState();

    return bestGame;
}

} // namespace handler
} // namespace player
} // namespace core
