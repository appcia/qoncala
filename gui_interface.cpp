#include "gui_interface.h"
#include "gui_bowl.h"
#include "gui_button.h"
#include "core_game.h"
#include "core_exception.h"
#include "core_player.h"
#include "core_action_nextplayer.h"

namespace gui {

Interface::Interface(core::Interface * model, QGraphicsObject *parent) :
    View(parent)
{
    name = "Interface";

    this->model = model;

    core::Game * game = model->getGame();

    if (!model->isVisible()) {
        return;
    }

    {
        core::Player * player = game->getPlayer(core::Player::N_First);
        Bowl * bowl = new Bowl(player->getScoreBowl(), this);
        bowl->setStoneColor(player->getStoneColor());
        bowl->setPos(-400, 325);
    }

    {
        core::Player * player = game->getPlayer(core::Player::N_Second);
        Bowl * bowl = new Bowl(player->getScoreBowl(), this);
        bowl->setStoneColor(player->getStoneColor());
        bowl->setPos(400, 325);
    }

    Button * nextButton = new Button(model, this);
    nextButton->setSize(QRectF(-125, -40, 250, 80));
    nextButton->setText("Next player");
    nextButton->setPos(0, 325);

    connect(nextButton, SIGNAL(pressed()), this, SLOT(nextButtonPressed()));
}

QRectF Interface::boundingRect() const
{
    return QRect();
}

void Interface::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void Interface::nextButtonPressed()
{
    if (!model->getGame()->isEnded() && model->getGame()->getCurrentCondition()->hasAction(core::Action::N_CaptureStone)) {
        core::action::NextPlayer * action = new core::action::NextPlayer();
        model->getGame()->resolveCurrentAction(action);
    }
}

} // namespace gui
