#ifndef CORE_PLAYER_HANDLER_RANDOM_H
#define CORE_PLAYER_HANDLER_RANDOM_H

#include "core_player_handler.h"

namespace core {
namespace player {
namespace handler {

class Random : public Handler
{
public:
    Random();

    Game * findBestGame();
};

} // namespace handler
} // namespace player
} // namespace core

#endif // CORE_PLAYER_HANDLER_RANDOM_H
