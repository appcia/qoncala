#ifndef GUI_BUTTON_H
#define GUI_BUTTON_H

#include "gui_view.h"
#include "core_interface.h"

namespace gui {

class Button : public View
{
    Q_OBJECT
public:
    explicit Button(core::Interface * model, QGraphicsObject* parent);

    void setText(QString text);
    QString getText() const;

    void setSize(QRectF size);
    QRectF getSize() const;

signals:
    void pressed();

public slots:

protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    core::Interface * model;

    QString text;
    QRectF size;
};

} // namespace gui

#endif // GUI_BUTTON_H
