#ifndef CORE_BOWL_H
#define CORE_BOWL_H

#include "core_stone.h"

#include <QtCore>

namespace core
{

class Board;

class Bowl
{
public:
    static const short Custom = -1;
    static const short Capacity = 9;

    static const short Position_Count = 6;
    enum Position {
        P_Custom = -1,
        P_Left = 0,
        P_UpperLeft = 1,
        P_UpperRight = 2,
        P_Right = 3,
        P_LowerRight = 4,
        P_LowerLeft = 5
    };

    explicit Bowl(Board * board, short num);
    explicit Bowl();
    virtual ~Bowl();

    bool isEqual(Bowl * bowl) const;

    void setBoard(Board * board);
    Board * getBoard() const;

    short getNum();
    bool isCustom();

    void setNeighbour(Bowl * bowl, Position num);
    QVector<Bowl *> getNeighbours(bool onlyExisting = false) const;
    Bowl * getNeighbour(Position position) const;
    Bowl * getNeighbour(short position) const;

    bool isFull();

    void addStone(Stone * stone);
    bool hasStone(Stone::Color color);
    Stone * getStone(short num);
    Stone * getStone(Stone::Color color);
    void removeStone(Stone * stone);
    void clearStones();
    QVector<Stone *> getStones(bool onlyUnaffected = false) const;
    QVector<Stone::Color> getStonesByColor() const;

    short getStoneCount() const;

    void setSelected(bool selected);
    bool isSelected() const;

    void setHighlighted(bool value);
    bool isHighlighted() const;
private:
    Board * board;

    short num;
    bool selected;
    bool highlighted;
    QVector<Bowl *> neighbours;
    QVector<Stone *> stones;
};

}

#endif // CORE_BOWL_H
