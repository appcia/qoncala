#ifndef CORE_ACTION_CLEARBOARD_H
#define CORE_ACTION_CLEARBOARD_H

#include "core_action.h"

namespace core {
namespace action {

class ClearBoard : public Action
{
public:
    ClearBoard();

    void execute();
    void undo();
    bool isLegal();

private:
    QVector<int> affectedStones;
    int selectedBowl;
};

} // namespace action
} // namespace core

#endif // CORE_ACTION_CLEARBOARD_H
