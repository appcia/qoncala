#ifndef CORE_INTERFACE_H
#define CORE_INTERFACE_H

namespace core {

class Game;

class Interface
{
public:
    explicit Interface(Game * game);

    Game * getGame() const;

    bool isVisible() const;
    void setVisible(bool visible);

private:
    Game * game;
    bool visible;
};

} // namespace core

#endif // CORE_INTERFACE_H
