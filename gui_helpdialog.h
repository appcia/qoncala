#ifndef GUI_HELPDIALOG_H
#define GUI_HELPDIALOG_H

#include <QDialog>

namespace gui {

namespace Ui {
class HelpDialog;
}

class HelpDialog : public QDialog
{
    Q_OBJECT
    
public:
    enum Page {
        P_GameRules,
        P_Authors,
        P_License
    };

    explicit HelpDialog(QWidget *parent = 0);
    ~HelpDialog();

public slots:
    void loadPage(int index);
    
private:
    Ui::HelpDialog *ui;

    QString pages[3];
};


} // namespace gui
#endif // GUI_HELPDIALOG_H
