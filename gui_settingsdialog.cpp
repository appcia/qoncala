#include "gui_settingsdialog.h"
#include "ui_gui_settingsdialog.h"

#include <QSettings>
#include <QDebug>

namespace gui
{

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    // @todo Remove following 2 lines if alpha-beta will be correctly reimplemented
    ui->blackPlayerIterationsSpin->setVisible(false);
    ui->whitePlayerIterationsSpin->setVisible(false);

    setupEvents();
    loadSettings();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::setupEvents()
{
    connect(ui->buttonBox, SIGNAL(accepted()), SLOT(saveSettings()));
    connect(ui->buttonBox, SIGNAL(rejected()), SLOT(loadSettings()));
    connect(ui->blackPlayerCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(blackPlayerChange(int)));
    connect(ui->whitePlayerCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(whitePlayerChange(int)));
}

void SettingsDialog::loadSettings()
{
    QSettings s;

    if (s.contains("startingPlayer")) {
        ui->startingPlayerCombo->setCurrentIndex(s.value("startingPlayer").toInt());
    }

    if (s.contains("blackPlayer")) {
        ui->blackPlayerCombo->setCurrentIndex(s.value("blackPlayer").toInt());
    }

    if (s.contains("blackPlayerIterations")) {
        ui->blackPlayerIterationsSpin->setValue(s.value("blackPlayerIterations").toInt());
    }

    if (s.contains("whitePlayer")) {
        ui->whitePlayerCombo->setCurrentIndex(s.value("whitePlayer").toInt());
    }

    if (s.contains("whitePlayerIterations")) {
        ui->whitePlayerIterationsSpin->setValue(s.value("whitePlayerIterations").toInt());
    }
}

void SettingsDialog::saveSettings()
{
    QSettings s;

    s.setValue("startingPlayer", ui->startingPlayerCombo->currentIndex());

    s.setValue("blackPlayer", ui->blackPlayerCombo->currentIndex());
    s.setValue("blackPlayerIterations", ui->blackPlayerIterationsSpin->value());

    s.setValue("whitePlayer", ui->whitePlayerCombo->currentIndex());
    s.setValue("whitePlayerIterations", ui->whitePlayerIterationsSpin->value());

    s.setValue("settingsActive", true);
}

void SettingsDialog::blackPlayerChange(int index)
{
    bool flag = index > 2;

    ui->blackPlayerIterationsSpin->setEnabled(flag);
    ui->blackPlayerIterationsSpin->setVisible(flag);
}

void SettingsDialog::whitePlayerChange(int index)
{
    bool flag = index > 2;

    ui->whitePlayerIterationsSpin->setEnabled(flag);
    ui->whitePlayerIterationsSpin->setVisible(flag);
}

}
