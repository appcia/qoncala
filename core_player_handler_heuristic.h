#ifndef CORE_PLAYER_HANDLER_HEURISTIC_H
#define CORE_PLAYER_HANDLER_HEURISTIC_H

#include "core_player_handler.h"

namespace core {
namespace player {
namespace handler {

class Heuristic : public Handler
{
public:
    Heuristic();

    core::Game * findBestGame();
};

} // namespace handler
} // namespace player
} // namespace core

#endif // CORE_PLAYER_HANDLER_HEURISTIC_H
