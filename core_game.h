#ifndef CORE_GAME_H
#define CORE_GAME_H

#include <QtCore>

#include "core_action.h"
#include "core_interface.h"
#include "core_board.h"
#include "core_player.h"
#include "core_gameworker.h"
#include "core_gamestate.h"
#include "core_player_handler.h"

namespace core
{

class Game
{
public:
    enum State {
        S_START = 0,
        S_SELECT = 1,
        S_MOVE = 2,
        S_CAPTURE = 3,
        S_END = 4
    };

    explicit Game();
    virtual ~Game();

    void setWorker(GameWorker * worker);
    GameWorker * getWorker();

    void setState(State state);
    State getState() const;

    bool isStarted();
    bool isEnded();

    void setBoard(Board * board);
    Board * getBoard() const;

    void setInterface(Interface * ui);
    Interface * getInterface() const;

    void setPlayer(Player * player);
    void setPlayer(Player::No no, Player * player);
    Player * getPlayer(Player::No no);
    Player::No getPlayerNo(Player * search) const;
    Player * getCurrentPlayer() const;
    Player * getOppositePlayer() const;
    Player::No getCurrentPlayerNo() const;
    void setCurrentPlayer(Player::No no);
    void switchCurrentPlayer();

    void diff(Game * origin);
    bool isEqual(const Game * g2);
    uint getKey();

    Condition * getCurrentCondition() const;
    void notifyNewCondition();

    Action * getNextAction() const;
    Action * getPreviousAction() const;
    void addAction(Action * action);

    void doAction(Action * action);
    void resolveCurrentAction(Action * action);
    void completeCurrentAction();
    void skipCurrentAction();
    void abortCurrentAction();

    QVector<Action *> getTodoActions() const;
    QVector<Action *> getDoneActions() const;

    Game * copy();
    static void clean();
private:
    State state;
    GameWorker * worker;

    Interface * ui;
    Board * board;

    Player::No currentPlayer;
    Player * players[2];

    QVector<Action *> todoActions;
    QVector<Action *> doneActions;

    static QList<Game *> copies;
};

}

#endif // CORE_GAME_H
