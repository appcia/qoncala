#include "core_action_capturestone.h"
#include "core_exception.h"
#include "core_game.h"

namespace core
{

namespace action
{

CaptureStone::CaptureStone(short bowlFrom, short stone)
{
    name = N_CaptureStone;

    this->bowlFrom = bowlFrom;
    this->stone = stone;
}

short CaptureStone::getBowlFrom() const
{
    return bowlFrom;
}

void CaptureStone::execute()
{
    Player * p = game->getCurrentPlayer();
    Bowl * b = game->getBoard()->getBowl(bowlFrom);
    Stone * s = game->getBoard()->getStone(stone);

    b->removeStone(s);
    p->getScoreBowl()->addStone(s);
}

bool CaptureStone::isLegal()
{
    Bowl * b = game->getBoard()->getBowl(bowlFrom);
    Stone * s = game->getBoard()->getStone(stone);

    Stone::Color color = s->getColor();
    QVector<Bowl *> neighbours = b->getNeighbours();

    for(short i = 0; i < 3; i++){
        short stonesInRow = 1;
        if (neighbours[i] && neighbours[i]->hasStone(color)) {
            stonesInRow++;
            if (neighbours[i]->getNeighbour(i) && neighbours[i]->getNeighbour(i)->hasStone(color)) {
                stonesInRow++;
            }
        }

        if (neighbours[i+3] != NULL && neighbours[i+3]->hasStone(color)) {
            stonesInRow++;
            if (neighbours[i+3]->getNeighbour(i+3) && neighbours[i+3]->getNeighbour(i+3)->hasStone(color)) {
                stonesInRow++;
            }
        }

        if (stonesInRow >= 3) {
            return true;
        }
    }

    return false;
}

void CaptureStone::undo()
{
    Player * p = game->getCurrentPlayer();
    Bowl * b = game->getBoard()->getBowl(bowlFrom);
    Stone * s = game->getBoard()->getStone(stone);

    p->getScoreBowl()->removeStone(s);
    b->addStone(s);
}

short CaptureStone::getStone() const
{
    return stone;
}

} //namespace action

} //namespace core
