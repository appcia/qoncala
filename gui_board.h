#ifndef GUI_BOARD_H
#define GUI_BOARD_H

#include "gui_view.h"
#include "gui_bowl.h"
#include "core_board.h"
#include "core_bowl.h"

namespace gui {

class Board : public View
{
    Q_OBJECT
public:
    Board(core::Board * model, QGraphicsObject *parent = 0);

signals:
    
public slots:

protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

private:
    core::Board * model;
};

} // namespace gui

#endif // GUI_BOARD_H
