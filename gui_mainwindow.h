#ifndef GUI_MAINWINDOW_H
#define GUI_MAINWINDOW_H

#include <QMainWindow>

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QLabel>
#include <QProgressBar>

#include "core_game.h"
#include "gui_settingsdialog.h"
#include "gui_helpdialog.h"

namespace gui
{

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void checkSettings();
    void openSettingsDialog();
    void openHelpDialog();

    void loadGameFromFile();
    void saveGameToFile();

    void startNewGame();
    void cancelGame();

    void updateGameScene();
    void updateGameStatus();

    void showProgressBar();
    void hideProgressBar();

private:
    Ui::MainWindow *ui;
    SettingsDialog * settingsDialog;
    HelpDialog * helpDialog;

    QGraphicsScene * scene;
    QGraphicsView * view;

    QLabel * statusLabel;
    QProgressBar * progressBar;

    void setupEvents();
    void setupGame(core::Game * game);
};

}

#endif // GUI_MAINWINDOW_H
