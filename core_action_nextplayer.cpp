#include "core_action_nextplayer.h"
#include "core_game.h"

namespace core
{

namespace action
{

NextPlayer::NextPlayer()
{
    name = N_NextPlayer;
}

void NextPlayer::execute()
{
    game->switchCurrentPlayer();
}

bool NextPlayer::isLegal()
{
    return game->getState() == Game::S_CAPTURE;
}

void NextPlayer::undo()
{
    game->switchCurrentPlayer();
}

}

}
