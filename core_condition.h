#ifndef CORE_CONDITION_H
#define CORE_CONDITION_H

#include <QtCore>
#include "core_action.h"

namespace core {

class Condition
{
public:
    Condition();

    void addAction(Action::Name name);
    void addIterableAction(Action::Name name, int iterations);
    void addContinousAction(Action::Name name);

    bool isIterableAction(Action::Name name);
    void iterateAction(Action::Name name);

    bool hasAction(Action::Name name);

    bool isDone() const;

private:
    QMap<Action::Name, int> actions;
};

} // namespace core

#endif // CORE_CONDITION_H
