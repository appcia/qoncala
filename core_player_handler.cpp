#include "core_player_handler.h"
#include "core_game.h"

#include "core_action_selectbowl.h"
#include "core_action_movestone.h"
#include "core_action_capturestone.h"
#include "core_action_nextplayer.h"
#include "core_action_clearboard.h"

namespace core {
namespace player {

Handler::Handler()
{
    this->player = player;
    rootState = NULL;
}

Handler::~Handler()
{
    if (rootState) {
        delete rootState;
        rootState = NULL;
    }
}

Handler::Type Handler::getType() const
{
    return type;
}

QString Handler::getTypeName() const
{
    switch (type) {
    case T_Random:
        return "Random";
        break;
    case T_Heuristic:
        return "Heuristic";
        break;
    case T_AlfaBeta:
        return "Alfa-beta";
    default:
        return "Unknown";
    }
}

void Handler::openRootState()
{
    if (rootState) {
        delete rootState;
        rootState = NULL;
    }

    rootState = new GameState(Controller::instance().getGame()->copy());
}

void Handler::deleteRootState()
{
    if (rootState) {
        delete rootState;
        rootState = NULL;
    }
}

void Handler::closeGameState(Game * game)
{
    Action * action = new action::NextPlayer();

    action->init(game);
    game->doAction(action);
    game->setState(Game::S_SELECT);
}

void Handler::generateChildren(GameState * node, QVector<Action *> actions)
{
    foreach (Action * action, actions) {
        Game * newGame = node->getGame()->copy();

        action->init(newGame);
        newGame->doAction(action);

        GameState * child = new GameState(newGame);
        node->addChild(child);
    }
}

QVector<Action *> Handler::getPossibleSelections(Game * game)
{
    QVector<Action *> possibleActions;

    Stone::Color playerColor = game->getCurrentPlayer()->getStoneColor();
    QVector<Bowl *> allBowls = game->getBoard()->getBowls();

    foreach (Bowl * bowl, allBowls) {
        if (bowl->hasStone(playerColor)) {
            possibleActions.push_back(new action::SelectBowl(bowl->getNum()));
        }
    }

    return possibleActions;
}

QVector<Action *> Handler::getPossibleMoves(Game * game)
{
    QVector<Action *> possibleActions;

    Bowl * from = game->getCurrentPlayer()->getSelectedBowl();
    QVector<Stone *> stones = from->getStones(true);
    QVector<Bowl *> neighbours;

    action::MoveStone * previousMove = static_cast<action::MoveStone *>(game->getPreviousAction());

    if (previousMove && previousMove->getName() == Action::N_MoveStone) {
        neighbours = game->getBoard()->getBowl(previousMove->getBowlTo())->getNeighbours(true);
    } else {
        neighbours = from->getNeighbours(true);
    }

    foreach (Stone * stone, stones) {
        foreach (Bowl * to, neighbours) {
            possibleActions.push_back(new action::MoveStone(from->getNum(), to->getNum(), stone->getNum()));
        }
    }

    return possibleActions;
}

QVector<Action *> Handler::getPossibleCaptures(Game * game)
{
    QVector<Action *> possibleActions;
    QVector<Bowl *> line;
    Stone::Color c;

    for (int i = 0; i < 6; i++) {
        line = game->getBoard()->getLine(i);
        for (int j = Stone::Iter_Begin; j <= Stone::Iter_End; j++) {
            c = (Stone::Color) j;

            if (line[0]->hasStone(c) && line[1]->hasStone(c) && line[2]->hasStone(c) && line[3]->hasStone(c)) {
                possibleActions.push_back(new action::CaptureStone(line[0]->getNum(), line[0]->getStone(c)->getNum()));
                possibleActions.push_back(new action::CaptureStone(line[1]->getNum(), line[1]->getStone(c)->getNum()));
                possibleActions.push_back(new action::CaptureStone(line[2]->getNum(), line[2]->getStone(c)->getNum()));
                possibleActions.push_back(new action::CaptureStone(line[3]->getNum(), line[3]->getStone(c)->getNum()));

                return possibleActions;
            } else if (line[0]->hasStone(c) && line[1]->hasStone(c) && line[2]->hasStone(c)) {
                possibleActions.push_back(new action::CaptureStone(line[0]->getNum(), line[0]->getStone(c)->getNum()));
                possibleActions.push_back(new action::CaptureStone(line[1]->getNum(), line[1]->getStone(c)->getNum()));
                possibleActions.push_back(new action::CaptureStone(line[2]->getNum(), line[2]->getStone(c)->getNum()));

                return possibleActions;
            } else if (line[1]->hasStone(c) && line[2]->hasStone(c) && line[3]->hasStone(c)) {
                possibleActions.push_back(new action::CaptureStone(line[1]->getNum(), line[1]->getStone(c)->getNum()));
                possibleActions.push_back(new action::CaptureStone(line[2]->getNum(), line[2]->getStone(c)->getNum()));
                possibleActions.push_back(new action::CaptureStone(line[3]->getNum(), line[3]->getStone(c)->getNum()));

                return possibleActions;
            }
        }
    }

    return possibleActions;
}

QVector<Game *> Handler::getAllPossibleGames(GameState * root)
{
    QVector<Game *> possibleGames;
    QHash<int, Game *> distinctGames;

    generateNextGames(root, possibleGames, distinctGames);

    std::random_shuffle(possibleGames.begin(),possibleGames.end());

    return possibleGames;
}

bool Handler::isDuplicate(Game * game, QHash<int, Game *> distinctGames)
{
    if (distinctGames.size() == 0) {
        return false;
    }

    QHash<int, Game *>::iterator i = distinctGames.find(game->getKey());
    while ((i != distinctGames.end()) && ((uint) i.key() == game->getKey())) {
        if (game->isEqual(i.value())) {
             return true;
        }
        ++i;
    }

    return false;
}

void Handler::generateNextGames(GameState * node, QVector<core::Game *> & possibleStates, QHash<int, Game *> & distinctStates)
{
    QVector<Action *> possibleActions;
    Game * game = node->getGame();

    switch (game->getState()) {
    case Game::S_SELECT:
        possibleActions = getPossibleSelections(game);
        game->setState(Game::S_MOVE);
        generateChildren(node, possibleActions);
        break;
    case Game::S_MOVE:
        if (!game->getCurrentPlayer()->getSelectedBowl()->getStones(true).isEmpty()) {
            possibleActions = getPossibleMoves(game);
            generateChildren(node, possibleActions);
        } else {
            if (!isDuplicate(game, distinctStates)){
                distinctStates.insertMulti(game->getKey(), game);
                core::Action * action = static_cast<core::Action *>(new core::action::ClearBoard());
                action->init(game);
                game->doAction(action);
                game->setState(core::Game::S_CAPTURE);
                generateNextGames(node, possibleStates, distinctStates);
            }
            return ;
        }
        break;
    case Game::S_CAPTURE:
        possibleActions = getPossibleCaptures(game);
        if (!possibleActions.isEmpty() && !game->isEnded()) {
            generateChildren(node, possibleActions);
        } else {
            possibleStates.push_back(game);
            return ;
       }
       break;
    default:
        break;
    }

    foreach (GameState * child, node->getChildren()) {
        generateNextGames(child, possibleStates, distinctStates);
    }
}

int Handler::evaluateGame(Game * game)
{
    int value = 0;

    const int Capture = 50;
    const int OwnStone = 2;
    const int EnemyStone = 1;
    const int GameEnd = 1000;

    Game * prevGame = Controller::instance().getGame();

    // terminal state
    if (game->isEnded()) {
        value = GameEnd - game->getDoneActions().count();
    } else {
        // Prefer captures
        value += (game->getCurrentPlayer()->getScore() - prevGame->getCurrentPlayer()->getScore()) * Capture;
        foreach (Bowl * bowl, game->getBoard()->getBowls()) {
            // Prefer more stones in your control
            if (bowl->hasStone(game->getCurrentPlayer()->getStoneColor()) && !bowl->hasStone(game->getOppositePlayer()->getStoneColor())) {
                value += bowl->getStoneCount() * OwnStone;
            // Prefer captures from enemy bowl
            } else if (bowl->hasStone(game->getOppositePlayer()->getStoneColor())) {
                value -= bowl->getStoneCount() * EnemyStone;
            }
        }
    }

    return value;
}

} // namespace player
} // namespace core

