#ifndef GUI_GUI_BOWL_H
#define GUI_GUI_BOWL_H

#include "gui_view.h"
#include "core_bowl.h"

namespace gui {

class Bowl : public View
{
    Q_OBJECT
public:
    explicit Bowl(core::Bowl * model, QGraphicsObject *parent = 0);

    void setStoneColor(core::Stone::Color stoneColor);
    
signals:
    
public slots:

protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    void dropEvent(QGraphicsSceneDragDropEvent *event);
    void dragEnterEvent(QGraphicsSceneDragDropEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    core::Bowl * model;
    core::Stone::Color stoneColor;
};

} // namespace gui

#endif // GUI_GUI_BOWL_H
