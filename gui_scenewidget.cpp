#include "gui_scenewidget.h"
#include "core_controller.h"

namespace gui
{

SceneWidget::SceneWidget(QWidget *parent) :
    QGraphicsView(parent)
{
    scene = new QGraphicsScene();
    scene->setBackgroundBrush(QBrush(QColor(191,145,74), Qt::SolidPattern));

    mapToScene(viewport()->rect().center());

    setScene(scene);
    setRenderHint(QPainter::Antialiasing);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

SceneWidget::~SceneWidget()
{
    if (scene) {
        delete scene;
    }
}

void SceneWidget::resizeEvent(QResizeEvent * event)
{
    Q_UNUSED(event);

    QPointF topleft = mapToScene(viewport()->rect().topLeft());

    resetMatrix();

    QPointF shift = (mapToScene(viewport()->rect().bottomRight() + QPoint(15, 10)) - mapToScene(viewport()->rect().topLeft()));
    shift /= (double) height() / scene->height();

    fitInView(QRectF(topleft, topleft + shift));
}

void SceneWidget::setGame(core::Game *game)
{
    this->game = game;
}

void SceneWidget::updateGame()
{
    scene->clear();

    if (game != NULL) {
        scene->addItem(new Board(game->getBoard()));
        scene->addItem(new Interface(game->getInterface()));
    }

    scene->update();

    // Hack for scaling on startup (triggering resize event)
    resizeEvent(new QResizeEvent(size(), size()));
}

}
