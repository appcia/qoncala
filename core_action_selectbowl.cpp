#include "core_action_selectbowl.h"
#include "core_exception.h"
#include "core_game.h"

namespace core
{

namespace action
{

SelectBowl::SelectBowl(short bowl)
{
    name = N_SelectBowl;

    this->bowl = bowl;
}

void SelectBowl::execute()
{
    Player * p = game->getCurrentPlayer();
    Bowl * b = game->getBoard()->getBowl(bowl);

    b->setSelected(true);
    p->selectBowl(b);
}

bool SelectBowl::isLegal()
{
    Player * p = game->getCurrentPlayer();
    Bowl * b = game->getBoard()->getBowl(bowl);

    return b->hasStone(p->getStoneColor());
}

short SelectBowl::getBowl() const
{
    return bowl;
}

void SelectBowl::undo()
{
    Player * p = game->getCurrentPlayer();
    Bowl * b = game->getBoard()->getBowl(bowl);

    b->setSelected(false);
    p->deselectBowl();
}

} //namespace action

} //namespace core
