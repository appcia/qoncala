#include "core_action_movestone.h"
#include "core_exception.h"
#include "core_game.h"

namespace core
{

namespace action
{

MoveStone::MoveStone(short from, short to, short stone)
{
    name = N_MoveStone;

    this->bowlFrom = from;
    this->bowlTo = to;
    this->stone = stone;
}

void MoveStone::execute()
{
    Player * p = game->getCurrentPlayer();
    Stone * s = game->getBoard()->getStone(stone);
    Bowl * f = game->getBoard()->getBowl(bowlFrom);
    Bowl * t = game->getBoard()->getBowl(bowlTo);

    f->removeStone(s);
    t->addStone(s);
    p->addAffectedStone(s);
}

bool MoveStone::isLegal()
{
    MoveStone * previousMove = static_cast<MoveStone *> (game->getPreviousAction());
    Bowl * f = game->getBoard()->getBowl(bowlFrom);
    Bowl * t = game->getBoard()->getBowl(bowlTo);

    if (previousMove) {
        QVector<Bowl *> neighbours;
        if (previousMove->getName() == Action::N_MoveStone){
            Bowl * pt = game->getBoard()->getBowl(previousMove->getBowlTo());
            neighbours = pt->getNeighbours();
        } else if (previousMove->getName() == Action::N_SelectBowl) {
            neighbours = f->getNeighbours();
        }

        foreach (Bowl * bowl, neighbours) {
            if (bowl == t && !bowl->isFull()) {
                return true;
            }
        }
    }

    return false;
}

void MoveStone::undo()
{
    Player * p = game->getCurrentPlayer();
    Stone * s = game->getBoard()->getStone(stone);
    Bowl * f = game->getBoard()->getBowl(bowlFrom);
    Bowl * t = game->getBoard()->getBowl(bowlTo);

    t->removeStone(s);
    f->addStone(s);
    p->removeAffectedStone(s);
}

short MoveStone::getBowlTo() const
{
    return bowlTo;
}

short MoveStone::getBowlFrom() const
{
    return bowlFrom;
}

short MoveStone::getStone() const
{
    return stone;
}

} //namespace action

} //namespace core
