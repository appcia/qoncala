#include "core_application.h"
#include "core_exception.h"

#include <QMessageBox>

namespace core {

Application::Application(int &argc, char *argv[]) :
    QApplication(argc, argv)
{
    setOrganizationDomain("appcia.linuxpl.info");
    setOrganizationName("appcia");
    setApplicationName("qoncala");
    setApplicationVersion("0.3");
}

bool Application::notify(QObject * receiver, QEvent * event)
{
    try {
        return QApplication::notify(receiver, event);
    }
    catch (core::Exception * exception) {
        QString code = "(" + QString::number(exception->getCode()) + ")";
        QString message = exception->getMessage();

        QMessageBox::warning(NULL, tr("Exception occured") + " " + code, message);
    }
    catch (std::exception & exception) {
        QMessageBox::warning(NULL, tr("Unknown exception occured"), exception.what());
    }

    return false;
}

} // namespace core
