#include "core_controller.h"
#include "core_player_handler_alfabetaht.h"

namespace core {
namespace player {
namespace handler {

AlfaBetaHT::AlfaBetaHT(int maxDepth)
{
    type = T_AlfaBetaHT;
    this->maxDepth = maxDepth;
}

bool compareStates(GameState * node1, GameState * node2)
{
    return Handler::evaluateGame(node1->getGame()) > Handler::evaluateGame(node2->getGame());
}

QPair<Game *, int> AlfaBetaHT::prune(GameState * node, int depth, QPair<Game *, int> alpha, QPair<Game *, int> beta)
{
    Game * game = node->getGame();
    Player::No player = game->getCurrentPlayerNo();

    if (depth == 0 || game->isEnded())
    {
        return qMakePair(game, evaluateGame(game));
    }

    QVector<GameState *> children = node->getChildren();
    qSort(children.begin(), children.end(), compareStates);
    node->addChildren(children);

    if (player == maxPlayer) {
        foreach (GameState * child, node->getChildren()) {
            QPair<Game *, int> value = prune(child, depth - 1, alpha, beta);
            if (alpha.second < value.second) {
                alpha = value;
            }

            if (alpha.second >= beta.second) {
                break;
            }
        }

        return alpha;
    } else {
        foreach (GameState * child, node->getChildren()) {
            QPair<Game *, int> value = prune(child, depth - 1, alpha, beta);
            if (beta.second > value.second) {
                beta = value;
            }

            if (alpha.second >= beta.second) {
                break;
            }
        }

        return beta;
    }
}



void AlfaBetaHT::createTree(GameState * node, int depth)
{
    if (depth > 0) {
        GameState * clone = new GameState(node->getGame()->copy());

        QVector<Game *> possibleGames = getAllPossibleGames(clone);

        foreach (Game * game, possibleGames) {
            GameState * child = new GameState(game->copy());
            node->addChild(child);
        }

        delete clone;

        if (--depth > 0) {
            foreach (GameState * child, node->getChildren()) {
                closeGameState(child->getGame());
                createTree(child, depth);
            }
        }
    }
}

Game * AlfaBetaHT::findBestGame()
{
    openRootState();

    maxPlayer = Controller::instance().getGame()->getCurrentPlayerNo();

    createTree(rootState, maxDepth);

    QPair<Game *, int> bestLeaf = prune(rootState, maxDepth, qMakePair(rootState->getGame(), INT_MIN), qMakePair(rootState->getGame(), INT_MAX));

    Game * bestGame = bestLeaf.first;
    bestGame = bestGame->copy();

    closeGameState(bestGame);

    deleteRootState();

    return bestGame;
}

} // namespace handler
} // namespace player
} // namespace core
