#include "core_gameworker.h"
#include "core_exception.h"
#include "core_game.h"
#include "core_player_ai.h"
#include "core_io.h"
#include "core_action_clearboard.h"

namespace core {

GameWorker::GameWorker(Game * game)
{
    if (game == NULL) {
        throw new Exception("Cannot work on game which is null");
    }

    this->game = game;
    this->condition = NULL;
}

void GameWorker::run()
{
    while (game != NULL && !game->isEnded()) {

        game->setState(Game::S_SELECT);
        Condition c1;
        c1.addAction(Action::N_SelectBowl);
        waitForCondition(&c1);

        game->setState(Game::S_MOVE);
        Condition c2;
        c2.addIterableAction(Action::N_MoveStone, game->getCurrentPlayer()->getSelectedBowl()->getStoneCount());
        waitForCondition(&c2);

        game->doAction(new action::ClearBoard());

        game->setState(Game::S_CAPTURE);
        Condition c3;
        c3.addContinousAction(Action::N_CaptureStone);
        c3.addAction(Action::N_NextPlayer);
        waitForCondition(&c3);
    }

    if (game == NULL) {
        throw new Exception("Game worker stopped, because game has become null");
    }

    game->setState(Game::S_END);
}

void GameWorker::waitForCondition(Condition * condition)
{
    if (condition == NULL) {
        throw new Exception("Cannot wait for condition which is null");
    }

    this->condition = condition;
    this->waiting = true;

    bool ai = game->getCurrentPlayer()->isAI();

    if (ai && game->getTodoActions().empty()) {
        emit aiProcessingStarted();

        qDebug() << "AI processing started";
        QTime time;
        time.start();

        player::AI * ai = static_cast<player::AI *> (game->getCurrentPlayer());
        ai->satisfyOwnCondition();

        game->clean();

        qDebug() << "AI processing ended, time elapsed: " << time.elapsed();

        emit aiProcessingEnded();
    }

    emit conditionWaiting();

    while (waiting) {
        ai ? usleep(AIDelay) : usleep(Delay);

        Action * action = game->getNextAction();

        if (action == NULL) {
            continue;
        }

        if (!condition->hasAction(action->getName())) {
            continue;
        }

        action->init(game);

        if (action->isLegal()) {
            action->execute();

            emit actionExecuted();

            game->completeCurrentAction();

            if (condition->isIterableAction(action->getName())) {
                condition->iterateAction(action->getName());
            }

            if (game->isEnded() || condition->isDone()) {
                break;
            }
        } else {
            game->skipCurrentAction();
        }
    }

    emit conditionDone();
}

bool GameWorker::isWaiting() const
{
    return waiting;
}

void GameWorker::setWaiting(bool flag)
{
    this->waiting = flag;
}

Condition *GameWorker::getCondition() const
{
    return condition;
}

}
