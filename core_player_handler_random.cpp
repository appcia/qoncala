#include "core_player_handler_random.h"
#include "core_condition.h"
#include "time.h"

namespace core {
namespace player {
namespace handler {

Random::Random()
{
    type = T_Random;
}

Game * Random::findBestGame()
{
    openRootState();

    qsrand((uint) time(NULL));
    QVector<Game *> possibleGames = getAllPossibleGames(rootState);
    Game * randomGame = possibleGames[qrand() % possibleGames.size()]->copy();

    closeGameState(randomGame);

    deleteRootState();

    return randomGame;
}

} // namespace handler
} // namespace player
} // namespace core
