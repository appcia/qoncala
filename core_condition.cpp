#include "core_condition.h"
#include "core_exception.h"

namespace core {

Condition::Condition()
{
}

void Condition::addAction(Action::Name name)
{
    if (actions.contains(name)) {
        throw new Exception("Action already exist");
    }

    actions[name] = 1;
}

void Condition::addIterableAction(Action::Name name, int iterations)
{
    if (actions.contains(name)) {
        throw new Exception("Action already exist");
    }

    if (iterations < 1) {
        throw new Exception("Iteration count must be greater than or equal to 1");
    }

    actions[name] = iterations;
}

void Condition::addContinousAction(Action::Name name)
{
    if (actions.contains(name)) {
        throw new Exception("Action already exist");
    }

    actions[name] = -1;
}

bool Condition::isIterableAction(Action::Name name)
{
    if (!actions.contains(name)) {
        return false;
    }

    return actions[name] >= 1;
}

void Condition::iterateAction(Action::Name name)
{
    if (!actions.contains(name)) {
        throw new Exception("Condition does not have action: " + name);
    }

    if (actions[name] > 0) {
        actions[name]--;
    }
}

bool Condition::hasAction(Action::Name name)
{
    return actions.contains(name);
}

bool Condition::isDone() const
{
    foreach (int iterations, actions) {
        if (iterations > 0) {
            return false;
        }
    }

    return true;
}

} // namespace core
