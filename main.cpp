#include <QApplication>
#include <QMessageBox>

#include "core_application.h"
#include "core_controller.h"
#include "gui_mainwindow.h"

int main(int argc, char *argv[])
{
    core::Controller::instance();
    core::Application app(argc, argv);

    gui::MainWindow window;
    window.show();

    return app.exec();
}
