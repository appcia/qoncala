#ifndef GUI_GUI_INTERFACE_H
#define GUI_GUI_INTERFACE_H

#include "gui_view.h"
#include "core_interface.h"

namespace gui {

class Interface : public View
{
    Q_OBJECT
public:
    explicit Interface(core::Interface * ui, QGraphicsObject * parent = 0);

signals:
    
public slots:
    void nextButtonPressed();

protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

private:
    core::Interface * model;
};

} // namespace gui

#endif // GUI_GUI_INTERFACE_H
