#include "core_exception.h"

namespace core {

Exception::Exception(QString message, int code)
{
    this->message = message;
    this->code = code;
}

Exception::~Exception() throw()
{
}

int Exception::getCode() const
{
    return code;
}

QString Exception::getMessage() const
{
    return message;
}

} // namespace core
