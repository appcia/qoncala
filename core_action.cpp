#include "core_action.h"
#include "core_game.h"
#include "core_exception.h"
#include "core_action_selectbowl.h"
#include "core_action_capturestone.h"
#include "core_action_clearboard.h"
#include "core_action_movestone.h"
#include "core_action_nextplayer.h"

namespace core
{

Action::Action()
{
    name = N_Default;
    game = NULL;
}

Action::Name Action::getName() const
{
    return name;
}

void Action::init(Game *game)
{
    this->game = game;
}

Action *Action::copy()
{
    Action * action = NULL;

    switch (name) {
    case N_SelectBowl:
        action = new action::SelectBowl(*((action::SelectBowl *) this));
        break;
    case N_MoveStone:
        action = new action::MoveStone(*((action::MoveStone *) this));
        break;
    case N_ClearBoard:
        action = new action::ClearBoard(*((action::ClearBoard *) this));
        break;
    case N_NextPlayer:
        action = new action::NextPlayer(*((action::NextPlayer *) this));
        break;
    case N_CaptureStone:
        action = new action::CaptureStone(*((action::CaptureStone *) this));
        break;
    default:
        throw new Exception("Invalid action to be copied");
        break;
    }

    // do not copy pointers (^ when making shallow copy)
    action->game = NULL;

    return action;
}

}
