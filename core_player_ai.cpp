#include "core_player_ai.h"
#include "core_player_handler.h"
#include "core_condition.h"
#include "core_game.h"

#include "core_io.h"

namespace core {
namespace player {

AI::AI(Game * game, Stone::Color stoneColor, Handler * handler) :
    Player(game, stoneColor)
{
    ai = true;

    this->handler = handler;
}

AI::~AI()
{
    if (handler) {
        delete handler;
        handler = NULL;
    }
}

void AI::satisfyOwnCondition()
{
    Game * bestGame = handler->findBestGame();

    if (bestGame != NULL) {
        bestGame->diff(game);
    }
}

} // namespace player
} // namespace core
