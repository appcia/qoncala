#ifndef CORE_PLAYER_AI_H
#define CORE_PLAYER_AI_H

#include "core_player.h"
#include "core_player_handler.h"

namespace core {
namespace player {

class AI : public Player
{
public:
    AI(Game * game, Stone::Color stoneColor, Handler * handler);
    virtual ~AI();

    void satisfyOwnCondition();
    void reactOnOthersCondition();

private:
    Handler * handler;

};

} // namespace player
} // namespace core

#endif // CORE_PLAYER_AI_H
