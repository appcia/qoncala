#include "gui_mainwindow.h"
#include "ui_gui_mainwindow.h"
#include "core_controller.h"
#include "core_io.h"

#include <QSettings>
#include <QDebug>
#include <QResizeEvent>
#include <QFileDialog>

namespace gui
{

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    settingsDialog = NULL;
    helpDialog = NULL;

    statusLabel = new QLabel(this);
    statusLabel->setText("Welcome in Qoncala!");
    ui->statusBar->addPermanentWidget(statusLabel);

    progressBar = new QProgressBar(this);
    progressBar->setMinimum(0);
    progressBar->setMaximum(0);
    progressBar->setValue(0);
    progressBar->setVisible(false);

    ui->statusBar->addPermanentWidget(progressBar);

    setupEvents();
    checkSettings();
}

MainWindow::~MainWindow()
{
    if (ui) {
        delete ui;
        ui = NULL;
    }

    if (statusLabel) {
        delete statusLabel;
        statusLabel = NULL;
    }

    if (progressBar) {
        delete progressBar;
        progressBar = NULL;
    }

    if (settingsDialog) {
        settingsDialog->saveSettings();

        delete settingsDialog;
        settingsDialog = NULL;
    }

    if (helpDialog) {
        delete helpDialog;
        helpDialog = NULL;
    }
}

void MainWindow::setupEvents()
{
    connect(ui->quitAction, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->settingsAction, SIGNAL(triggered()), this, SLOT(openSettingsDialog()));
    connect(ui->newGameAction, SIGNAL(triggered()), this, SLOT(startNewGame()));
    connect(ui->loadAction, SIGNAL(triggered()), this, SLOT(loadGameFromFile()));
    connect(ui->saveAction, SIGNAL(triggered()), this, SLOT(saveGameToFile()));
    connect(ui->helpAction, SIGNAL(triggered()), this, SLOT(openHelpDialog()));
    connect(ui->cancelAction, SIGNAL(triggered()), this, SLOT(cancelGame()));
}

void MainWindow::checkSettings()
{
    QSettings s;

    if (!s.contains("settingsActive")) {

        // Auto open on first run
        openSettingsDialog();

        // Save initial settings
        settingsDialog->saveSettings();
    }
}

void MainWindow::openSettingsDialog()
{
    if (settingsDialog == NULL) {
        settingsDialog = new SettingsDialog();
    }

    settingsDialog->show();
}

void MainWindow::openHelpDialog()
{
    if (helpDialog == NULL) {
        helpDialog = new HelpDialog();
    }

    helpDialog->show();
}

void MainWindow::saveGameToFile()
{
    core::Game * game = core::Controller::instance().getGame();
    core::IO * io = core::Controller::instance().getIO();

    QString filename = QFileDialog::getSaveFileName(this, tr("Specify game file to be saved"), QDir::homePath(), "*." + core::IO::ext);

    if (filename.isEmpty()) {
        return;
    }

    io->saveGameToFile(game, filename);
}

void MainWindow::loadGameFromFile()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Specify game file to be loaded"), QDir::homePath(), "*." + core::IO::ext);
    if (filename.isEmpty()) {
        return;
    }

    core::IO * io = core::Controller::instance().getIO();
    setupGame(io->loadGameFromFile(filename));
}

void MainWindow::startNewGame()
{
    core::IO * io = core::Controller::instance().getIO();
    setupGame(io->createNewGame());
}

void MainWindow::cancelGame()
{
    core::Controller::instance().endGame();

    ui->sceneWidget->setGame(NULL);
    ui->sceneWidget->updateGame();

    updateGameScene();
    updateGameStatus();
}

void MainWindow::setupGame(core::Game *game)
{
    core::Controller::instance().startGame(game);

    ui->sceneWidget->setGame(game);
    ui->sceneWidget->updateGame();

    connect(game->getWorker(), SIGNAL(actionExecuted()), this, SLOT(updateGameScene()));

    connect(game->getWorker(), SIGNAL(conditionWaiting()), this, SLOT(updateGameStatus()));
    connect(game->getWorker(), SIGNAL(actionExecuted()), this, SLOT(updateGameStatus()));
    connect(game->getWorker(), SIGNAL(conditionDone()), this, SLOT(updateGameStatus()));
    connect(game->getWorker(), SIGNAL(aiProcessingStarted()), this, SLOT(showProgressBar()));
    connect(game->getWorker(), SIGNAL(aiProcessingEnded()), this, SLOT(hideProgressBar()));

    updateGameScene();
    updateGameStatus();
}

void MainWindow::updateGameScene()
{
    ui->sceneWidget->updateGame();
}

void MainWindow::updateGameStatus()
{
    QString status;
    bool saveable = false;

    core::Game * game = core::Controller::instance().getGame();

    if (game != NULL) {
        core::Player * player = game->getCurrentPlayer();

        switch (game->getState()) {
        case core::Game::S_START:
            status = player->getName() + " - " + "Welcome to Qoncala!";
            break;
        case core::Game::S_SELECT:
            status = player->getName() + " - " + "Selecting bowl";
            break;
        case core::Game::S_MOVE:
            status = player->getName() + " - " + "Stone distribution";
            break;
        case core::Game::S_CAPTURE:
            status = player->getName() + " - " + "Capturing stones";
            break;
        case core::Game::S_END:
            status = player->getName() + " - " + "Wins";
            break;
        default:
            status = "Unknown state";
            break;
        }

        saveable = (game->getState() == core::Game::S_SELECT)
                && (game->getCurrentPlayer()->getAffectedStones().count() == 0)
                && (!game->getCurrentPlayer()->hasSelectedBowl());
    }
    else {
        status = "Welcome to Qoncala!";
    }

    statusLabel->setText(status);
    ui->saveAction->setEnabled(saveable);
}

void MainWindow::showProgressBar()
{
    progressBar->show();
    statusLabel->hide();
}

void MainWindow::hideProgressBar()
{
    progressBar->hide();
    statusLabel->show();
}

}
