#ifndef CORE_ACTION_H
#define CORE_ACTION_H

#include <QtCore>
#include "core_player.h"
#include "core_board.h"

namespace core
{

class Action
{
public:
    enum Name {
        N_Default,
        N_SelectBowl,
        N_MoveStone,
        N_ClearBoard,
        N_CaptureStone,
        N_NextPlayer
    };

    Action();
    virtual ~Action() {}

    Name getName() const;

    void init(Game * game);
    virtual void execute() = 0;
    virtual bool isLegal() = 0;
    virtual void undo() = 0;

    Action * copy();
protected:
    Name name;
    Game * game;
};

}

#endif // CORE_ACTION_H
