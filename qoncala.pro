#-------------------------------------------------
#
# Project created by QtCreator 2013-02-16T14:04:53
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qoncala
TEMPLATE = app


SOURCES += main.cpp gui_mainwindow.cpp \
    gui_settingsdialog.cpp \
    gui_scenewidget.cpp \
    core_game.cpp \
    core_player.cpp \
    core_stone.cpp \
    core_bowl.cpp \
    core_board.cpp \
    gui_board.cpp \
    gui_stone.cpp \
    gui_bowl.cpp \
    gui_interface.cpp \
    gui_view.cpp \
    core_interface.cpp \
    gui_stonedata.cpp \
    core_gameworker.cpp \
    core_action.cpp \
    core_action_selectbowl.cpp \
    core_action_movestone.cpp \
    core_action_capturestone.cpp \
    gui_button.cpp \
    core_condition.cpp \
    core_player_ai.cpp \
    core_player_handler.cpp \
    core_player_handler_random.cpp \
    core_controller.cpp \
    core_player_handler_alfabeta.cpp \
    core_player_handler_heuristic.cpp \
    core_io.cpp \
    core_exception.cpp \
    core_application.cpp \
    core_action_clearboard.cpp \
    gui_helpdialog.cpp \
    core_action_nextplayer.cpp \
    core_gamestate.cpp \
    core_player_handler_alfabetaht.cpp


HEADERS += gui_mainwindow.h \
    gui_settingsdialog.h \
    gui_scenewidget.h \
    core_game.h \
    core_player.h \
    core_stone.h \
    core_bowl.h \
    core_board.h \
    gui_board.h \
    gui_stone.h \
    gui_bowl.h \
    gui_interface.h \
    gui_view.h \
    core_interface.h \
    gui_stonedata.h \
    core_gameworker.h \
    core_action.h \
    core_action_selectbowl.h \
    core_action_movestone.h \
    core_action_capturestone.h \
    gui_button.h \
    core_condition.h \
    core_player_ai.h \
    core_player_handler.h \
    core_player_handler_random.h \
    core_controller.h \
    core_player_handler_alfabeta.h \
    core_player_handler_heuristic.h \
    core_io.h \
    core_exception.h \
    core_application.h \
    core_action_clearboard.h \
    gui_helpdialog.h \
    core_action_nextplayer.h \
    core_gamestate.h \
    core_player_handler_alfabetaht.h


FORMS += gui_mainwindow.ui \
    gui_settingsdialog.ui \
    gui_helpdialog.ui

RESOURCES += \
    icon.qrc \
    help.qrc
