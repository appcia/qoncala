#include "gui_board.h"
#include "core_exception.h"

#include <QGraphicsScene>
#include <QPainter>

namespace gui {

Board::Board(core::Board * model, QGraphicsObject *parent) :
    View(parent)
{
    this->model = model;

    name = "Board";

    foreach (core::Bowl * bowl, this->model->getBowls()) {
        Bowl * item = new Bowl(bowl, this);

        switch (bowl->getNum()) {
        case 10:
            item->setPos(-110, 0);
            break;
        case 11:
            item->setPos(-165, -100);
            break;
        case 12:
            item->setPos(-55, -100);
            break;
        case 1:
            item->setPos(0, -200);
            break;
        case 2:
            item->setPos(55, -100);
            break;
        case 3:
            item->setPos(165, -100);
            break;
        case 4:
            item->setPos(110, 0);
            break;
        case 5:
            item->setPos(165, 100);
            break;
        case 6:
            item->setPos(55, 100);
            break;
        case 7:
            item->setPos(0, 200);
            break;
        case 8:
            item->setPos(-55, 100);
            break;
        case 9:
            item->setPos(-165, 100);
            break;
        }
    }
}

QRectF Board::boundingRect() const
{
    return QRect();
}

void Board::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

} // namespace gui
