#include "core_game.h"
#include "core_player_ai.h"
#include "core_exception.h"
#include "core_io.h"

namespace core
{

QList<Game *> Game::copies;

Game::Game()
{
    state = S_START;
    worker = NULL;
    board = NULL;
    ui = NULL;
    currentPlayer = Player::N_First;

    for (int i = 0; i < Player::No_Count; i++) {
        players[i] = NULL;
    }
}

Game::~Game() {
    foreach (Action * action, todoActions) {
        delete action;
    }

    foreach (Action * action, doneActions) {
        delete action;
    }

    for (int i = 0; i < Player::No_Count; i++) {
        delete players[i];
    }

    delete board;
    delete ui;
}

uint Game::getKey()
{
    uint value = 0;

    QVector<Bowl *> bowls = board->getBowls();

    foreach (Bowl * bowl, bowls) {
        QVector<Stone *> stones = bowl->getStones();
        if (!stones.isEmpty()) {
            int sum = 0;
            foreach (Stone * stone, stones) {
                int color = (int)stone->getColor();
                sum += (color + 1);
            }
            value += bowl->getNum() * bowl->getNum() * sum * sum;
        }
    }

    return value;
}

void Game::setWorker(GameWorker *worker)
{
    if (worker == NULL) {
        throw new Exception("Game worker cannot be null");
    }

    this->worker = worker;
}

GameWorker *Game::getWorker()
{
    return worker;
}

void Game::setState(Game::State status)
{
    this->state = status;
}

Game::State Game::getState() const
{
    return state;
}

void Game::switchCurrentPlayer()
{
    int c = (int) currentPlayer;
    currentPlayer = (Player::No) ((++c) % Player::No_Count);
}

Condition * Game::getCurrentCondition() const
{
    if (worker == NULL) {
        throw new Exception("Game worker is not running");
    }

    return worker->getCondition();
}

Action *Game::getNextAction() const
{
    if (todoActions.size() > 0) {
        return todoActions[0];
    }

    return NULL;
}

Action *Game::getPreviousAction() const
{
    int index = doneActions.size() - 1;

    if (index >= 0) {
        return doneActions[index];
    }

    return NULL;
}

void Game::diff(Game * origin)
{
    int index = origin->getDoneActions().count();

    for (int i = doneActions.count() - 1; i >= index; i--) {
        if (doneActions[i]->getName() == Action::N_ClearBoard) {
           continue;
        }
        origin->resolveCurrentAction(doneActions[i]->copy());
    }
}

bool Game::isEqual(const Game * g2)
{
    QVector<Bowl *> bowls = board->getBowls();
    QVector<Bowl *> _bowls = g2->getBoard()->getBowls();

    for (int i = 0; i < bowls.size(); i++) {
        if (!bowls[i]->isEqual(_bowls[i])) {
            return false;
        }
    }

    return true;
}

/**
 * Add action to 'todo' list
 *
 * @param action
 */
void Game::addAction(Action *action)
{
    if (action == NULL) {
        throw new Exception("Action to be added cannot be null");
    }

    todoActions.push_back(action);
}

/**
 * Execute action without worker waiting
 *
 * @param action
 */
void Game::doAction(Action *action)
{
    if (action == NULL) {
        throw new Exception("Action to be executed cannot be null");
    }

    action->init(this);
    action->execute();

    doneActions.push_back(action);
}

/**
 * Force action to be current worker action
 *
 * @param action
 */
void Game::resolveCurrentAction(Action *action)
{
    if (action == NULL) {
        throw new Exception("Action to be resolved cannot be null");
    }

    todoActions.push_front(action);
}

/**
 * Set current action as completed, move from 'todo' to 'done' list
 */
void Game::completeCurrentAction()
{
    if (todoActions.size() == 0) {
        throw new Exception("There is no action to complete");
    }

    Action * action = todoActions.front();

    todoActions.pop_front();
    doneActions.push_back(action);
}

/**
 * Remove current action, useful when action is unlegal or unexpected
 */
void Game::skipCurrentAction()
{
    if (todoActions.size() == 0) {
        throw new Exception("There is no action to skip");
    }

    Action * action = todoActions.front();
    todoActions.pop_front();
    delete action;
}

/**
 * Worker will cancel waiting for current action
 */
void Game::abortCurrentAction()
{
    worker->setWaiting(false);
}

QVector<Action *> Game::getTodoActions() const
{
    return todoActions;
}

QVector<Action *> Game::getDoneActions() const
{
    return doneActions;
}

bool Game::isStarted()
{
    return worker != NULL;
}

bool Game::isEnded()
{
    for (int i = 0; i < Player::No_Count; i++) {
        if (players[i]->getScore() == 7) {
            return true;
        }
    }

    return false;
}

void Game::setBoard(Board *board)
{
    if (board == NULL) {
        throw new Exception("Board cannot be null");
    }

    this->board = board;
}

Player * Game::getCurrentPlayer() const
{
    return players[currentPlayer];
}

Player * Game::getOppositePlayer() const
{
    if (currentPlayer == Player::N_First) {
        return players[Player::N_Second];
    } else {
        return players[Player::N_First];
    }
}

Player::No Game::getCurrentPlayerNo() const
{
    return currentPlayer;
}

void Game::setCurrentPlayer(Player::No no)
{
    this->currentPlayer = no;
}

void Game::setPlayer(Player *player)
{
    if (player == NULL) {
        throw new Exception("Player cannot be null");
    }

    for (int i = 0; i < Player::No_Count; i++) {
        if (players[i] == NULL) {
            players[i] = player;
            return;
        }
    }

    throw new Exception("Cannot assign more players to game. Maximum exceeded: " + QString::number(Player::No_Count));
}

void Game::setPlayer(Player::No no, Player *player)
{
    if (player == NULL) {
        throw new Exception("Player no." + QString::number(no) + " cannot be null");
    }

    this->players[no] = player;
}

Player *Game::getPlayer(Player::No no)
{
    return players[no];
}

Player::No Game::getPlayerNo(Player * search) const
{
    for (int i = 0; i < Player::No_Count; i++) {
        if (players[i] == search) {
            return (Player::No) i;
        }
    }

    throw new Exception("Could not determine player no");
}

Board *Game::getBoard() const
{
    return board;
}

void Game::setInterface(Interface * ui)
{
    if (ui == NULL) {
        throw new Exception("Interface cannot be null");
    }

    this->ui = ui;
}

Interface *Game::getInterface() const
{
    return ui;
}

Game *Game::copy()
{
    IO * io = core::Controller::instance().getIO();

    Game * copy = io->createNewGame(false, false);
    Game * origin = this;

    copy->setState(origin->getState());

    foreach (Action * action, origin->getDoneActions()) {
        copy->doAction(action->copy());
    }

    copies << copy;

    return copy;
}

void Game::clean()
{
    foreach (Game * copy, copies) {
        delete copy;
    }

    copies.clear();
}

}
