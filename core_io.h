#ifndef CORE_IO_H
#define CORE_IO_H

#include "core_game.h"
#include "core_action.h"
#include "core_action_capturestone.h"
#include "core_action_clearboard.h"
#include "core_action_movestone.h"
#include "core_action_nextplayer.h"
#include "core_action_selectbowl.h"

namespace core {

class IO
{
public:
    const static QString ext;

    IO();

    Game * createNewGame(bool empty = false, bool flush = true);

    Game * loadGameFromFile(QString filename);
    void saveGameToFile(Game * game, QString filename);

    void flushSettings();

private:
    QString composeAction(Action * action);
    Action * parseAction(QString str);

    int blackPlayer;
    int blackPlayerIterations;

    int whitePlayer;
    int whitePlayerIterations;

    int startingPlayer;
};

} // namespace core

#endif // CORE_IO_H
