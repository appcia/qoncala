#ifndef GUI_SETTINGSDIALOG_H
#define GUI_SETTINGSDIALOG_H

#include <QDialog>

namespace gui
{

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

    enum Player {
        P_Human,
        P_Random,
        P_Heuristic,
        P_AlfaBeta,
        P_AlfaBetaHard
    };

    enum StartingPlayer {
        SP_Black,
        SP_White
    };

public slots:
    void loadSettings();
    void saveSettings();

    void blackPlayerChange(int index);
    void whitePlayerChange(int index);
    
private:
    Ui::SettingsDialog *ui;

    void setupEvents();
};

}

#endif // GUI_SETTINGSDIALOG_H
