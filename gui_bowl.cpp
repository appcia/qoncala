#include "gui_bowl.h"
#include "gui_stone.h"
#include "gui_stonedata.h"
#include "core_game.h"
#include "core_exception.h"
#include "core_player.h"
#include "core_stone.h"
#include "core_action_selectbowl.h"
#include "core_action_movestone.h"

namespace gui {

Bowl::Bowl(core::Bowl * model, QGraphicsObject *parent) :
    View(parent)
{
    name = "Bowl";

    this->model = model;

    if (!model->isCustom()) {
        setAcceptDrops(true);
        setFlags(QGraphicsItem::ItemIsSelectable);

        if (model->getBoard()->getGame()->getCurrentPlayer()->isAI()) {
            setEnabled(false);
        }
    }

    int i = 0;
    const int row = 3;
    const int d = (-1) * Stone::Size;

    foreach (core::Stone * stone, model->getStones()) {
        Stone * item = new Stone(stone, this);
        item->setPos(d + (i % row) * Stone::Size, d + (i / row) * Stone::Size);

        i++;
    }
}

void Bowl::setStoneColor(core::Stone::Color color)
{
    this->stoneColor = color;
}

QRectF Bowl::boundingRect() const
{
    return QRect(QPoint(-50, -50), QPoint(50, 50));
}

void Bowl::paint(QPainter * p, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);

    // Brush
    p->setBrush(QColor(model->isCustom() ? "#DBA85A" : "#FFC162"));

    // Pen
    QPen pen;
    pen.setWidth(3);

    if (model->isCustom()) {
        switch (stoneColor) {
        case core::Stone::C_Black:
            pen.setBrush(QColor("#000000"));
            break;
        case core::Stone::C_White:
            pen.setBrush(QColor("#ffffff"));
            break;
        default:
            break;
        }
    }
    else {
        if (model->isHighlighted()) {
            pen.setBrush(QColor("#7C7C7C"));
        } else if (model->isSelected()) {
            pen.setBrush(QColor("#00CC00"));
        } else {
            pen.setBrush(QColor("#FFC162"));
        }
    }
    p->setPen(pen);

    p->drawEllipse(QPoint(0, 0), 50, 50);
}

void Bowl::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    if (model->isCustom()) {
        return;
    }

    if (!model->isFull()) {
        event->setAccepted(true);
    } else {
        event->setAccepted(false);
    }
}

void Bowl::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    if (model->isCustom()) {
        return;
    }

    const StoneData * stoneData = qobject_cast<const StoneData *>(event->mimeData());
    if (!stoneData) {
        return;
    }

    core::Stone * stone = stoneData->getStone();
    core::Bowl * bowl = stone->getBowl();
    core::Game * game = bowl->getBoard()->getGame();

    core::action::MoveStone * action = new core::action::MoveStone(bowl->getNum(), model->getNum(), stone->getNum());
    game->resolveCurrentAction(action);
}

void Bowl::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);

    if (model->isCustom()) {
        return;
    }

    core::Game * game = model->getBoard()->getGame();

    if (game->getState() == core::Game::S_SELECT) {
        core::action::SelectBowl * action = new core::action::SelectBowl(model->getNum());
        game->resolveCurrentAction(action);
    }

}

} // namespace gui
