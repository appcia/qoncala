#include "core_io.h"
#include "core_player.h"
#include "core_player_ai.h"
#include "core_player_handler_random.h"
#include "core_player_handler_alfabeta.h"
#include "core_player_handler_alfabetaht.h"
#include "core_player_handler_heuristic.h"
#include "core_exception.h"
#include "core_action.h"

namespace core {

const QString IO::ext = "qoncala";

IO::IO()
{
    flushSettings();
}

void IO::flushSettings()
{
    QSettings s;

    blackPlayer = s.value("blackPlayer").toInt();
    blackPlayerIterations = s.value("blackPlayerIterations").toInt();

    whitePlayer = s.value("whitePlayer").toInt();
    whitePlayerIterations = s.value("whitePlayerIterations").toInt();

    startingPlayer = s.value("startingPlayer").toInt();
}

Game *IO::createNewGame(bool empty, bool flush)
{
    if (flush) {
        flushSettings();
    }

    Game * game = new Game();

    switch (blackPlayer) {
    case 1:
        new player::AI(game, Stone::C_Black, new player::handler::Random());
        break;
    case 2:
        new player::AI(game, Stone::C_Black, new player::handler::Heuristic);
        break;
    case 3:
        new player::AI(game, Stone::C_Black, new player::handler::AlfaBeta(blackPlayerIterations));
        break;
    case 4:
        new player::AI(game, Stone::C_Black, new player::handler::AlfaBetaHT(blackPlayerIterations));
        break;
    case 0:
    default:
        new Player(game, Stone::C_Black);
        break;
    }

    switch (whitePlayer) {
    case 1:
        new player::AI(game, Stone::C_White, new player::handler::Random());
        break;
    case 2:
        new player::AI(game, Stone::C_White, new player::handler::Heuristic());
        break;
    case 3:
        new player::AI(game, Stone::C_White, new player::handler::AlfaBeta(whitePlayerIterations));
        break;
    case 4:
        new player::AI(game, Stone::C_White, new player::handler::AlfaBetaHT(whitePlayerIterations));
        break;
    case 0:
    default:
        new Player(game, Stone::C_White);
        break;
    }

    game->setCurrentPlayer((Player::No) startingPlayer);

    Board * board = new Board(game);
    if (!empty) {
        board->prepare();
    }

    new Interface(game);

    return game;
}

Game *IO::loadGameFromFile(QString path)
{
    QSettings s(path, QSettings::IniFormat);

    s.sync();

    if (!QFile(s.fileName()).exists() || s.status() != QSettings::NoError) {
        throw new Exception("Load error. Game file does not exist:\n" + path);
    }

    Game * game = createNewGame();

    int count = s.value("game/done/count").toInt();
    for (int i = 0; i < count; i++) {
        QString val = s.value("game/done/action/" + QString::number(i)).toString();
        if (val.isEmpty()) {
            continue;
        }

        Action * action = parseAction(val);

        game->doAction(action);
    }

    return game;
}

void IO::saveGameToFile(Game * game, QString path)
{
    if (game == NULL) {
        throw new Exception("Cannot save game");
    }

    if (path.isEmpty()) {
        throw new Exception("Path to game file is empty");
    }

    path = path + "." + ext;
    QSettings s(path, QSettings::IniFormat);

    s.setValue("game/done/count", game->getDoneActions().count());

    int i = 0;
    foreach (Action * action, game->getDoneActions()) {
        QString val = composeAction(action);
        if (val.isEmpty()) {
            continue;
        }

        s.setValue("game/done/action/" + QString::number(i), val);
        i++;
    }

    s.sync();

    if (!QFile(s.fileName()).exists() || s.status() != QSettings::NoError) {
        throw new Exception("Save error. File could not be created:\n" + path);
    }
}

QString IO::composeAction(Action *action)
{
    int name = action->getName();

    QStringList val;
    val << QString::number(name);

    if (name == Action::N_CaptureStone) {
        action::CaptureStone * a = static_cast<action::CaptureStone *> (action);
        val << QString::number(a->getBowlFrom()) << QString::number(a->getStone());
    }
    else if (name == Action::N_ClearBoard) {
        // (no params)
    }
    else if (name == Action::N_MoveStone) {
        action::MoveStone * a = static_cast<action::MoveStone *> (action);
        val << QString::number(a->getBowlFrom()) << QString::number(a->getBowlTo()) << QString::number(a->getStone());
    }
    else if (name == Action::N_NextPlayer) {
        // (no params)
    }
    else if (name == Action::N_SelectBowl) {
        action::SelectBowl * a = static_cast<action::SelectBowl *> (action);
        val << QString::number(a->getBowl());
    }
    else {
        throw new Exception("Cannot compose action. Invalid name: " + QString::number(name));
    }

    QString str = val.join(" ");

    return str;
}

Action *IO::parseAction(QString str)
{
    QStringList val = str.split(" ");

    int name = val.at(0).toInt();
    Action * action = NULL;

    switch (name) {
    case Action::N_CaptureStone:
        action = new action::CaptureStone(val.at(1).toInt(), val.at(2).toInt());
        break;
    case Action::N_ClearBoard:
        action = new action::ClearBoard();
        break;
    case Action::N_MoveStone:
        action = new action::MoveStone(val.at(1).toInt(), val.at(2).toInt(), val.at(3).toInt());
        break;
    case Action::N_NextPlayer:
        action = new action::NextPlayer();
        break;
    case Action::N_SelectBowl:
        action = new action::SelectBowl(val.at(1).toInt());
        break;
    default:
        throw new Exception("Cannot parse action. Unrecognized name found: " + QString::number(name));
        break;
    }

    return action;
}

} // namespace core
