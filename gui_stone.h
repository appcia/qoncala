#ifndef GUI_GUI_STONE_H
#define GUI_GUI_STONE_H

#include "gui_view.h"
#include "core_stone.h"

namespace gui {

class Stone : public View
{
    Q_OBJECT
public:
    static const int Size = 20;

    explicit Stone(core::Stone * model, QGraphicsObject *parent = 0);
    
signals:
    
public slots:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);

protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
    core::Stone * model;

    void generatePossibleMoves();
    void highlightBowls(bool value);
    QVector<core::Bowl *> possibleMoves;
};

} // namespace gui

#endif // GUI_GUI_STONE_H
