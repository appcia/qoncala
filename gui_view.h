#ifndef GUI_VIEW_H
#define GUI_VIEW_H

#include <QGraphicsObject>
#include <QDebug>
#include <QtGui>

#include <QGraphicsScene>
#include <QPainter>
#include <QColor>

namespace gui {

class View : public QGraphicsObject
{
    Q_OBJECT

public:
    View(QGraphicsObject * parent = 0);

    QString getName() const;

protected:
    QString name;

private:
    
};

} // namespace gui

#endif // GUI_VIEW_H
