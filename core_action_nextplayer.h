#ifndef CORE_ACTION_NEXTPLAYER_H
#define CORE_ACTION_NEXTPLAYER_H

#include "core_action_capturestone.h"

namespace core
{

namespace action
{

class NextPlayer : public Action
{
public:
    NextPlayer();

    void execute();
    void undo();
    bool isLegal();
};

}

}

#endif // CORE_ACTION_NEXTPLAYER_H
