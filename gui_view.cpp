#include "gui_view.h"

namespace gui {

View::View(QGraphicsObject *parent) :
    QGraphicsObject(parent)
{
    name = "View";
}

QString View::getName() const
{
    return name;
}

} // namespace gui
