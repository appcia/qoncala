#ifndef GUI_SCENEWIDGET_H
#define GUI_SCENEWIDGET_H

#include <QGraphicsScene>
#include <QGraphicsView>

#include "gui_board.h"
#include "gui_interface.h"

namespace gui
{

class SceneWidget : public QGraphicsView
{
    Q_OBJECT
public:
    explicit SceneWidget(QWidget *parent = 0);
    ~SceneWidget();

    void resizeEvent(QResizeEvent *event);
    void setGame(core::Game * game);
    void updateGame();

protected:

private:
    QGraphicsScene * scene;
    core::Game * game;
};

}

#endif // GUI_SCENEWIDGET_H
