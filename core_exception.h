#ifndef CORE_EXCEPTION_H
#define CORE_EXCEPTION_H

#include <QtCore>

namespace core {

class Exception : public std::exception
{
public:
    Exception(QString message, int code = -1);
    virtual ~Exception() throw();

    int getCode() const;
    QString getMessage() const;

private:
    QString message;
    int code;
};

} // namespace core

#endif // CORE_EXCEPTION_H
