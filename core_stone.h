#ifndef CORE_STONE_H
#define CORE_STONE_H

#include <QtCore>

namespace core
{
class Bowl;

class Stone
{
public:
    enum Color {
        C_Black,
        C_White,
        C_Red,
        C_Yellow,
        C_Green,
        Iter_Begin = C_Black,
        Iter_End = C_Green
    };

    explicit Stone(Bowl * bowl, Color color, short num);

    Color getColor();
    QString getColorName();
    short getNum() const;

    Bowl * getBowl();
    void setBowl(Bowl * bowl);

    void setLocked(bool flag);
    bool isLocked();
private:
    Bowl * bowl;
    Color color;
    short num;
    bool locked;
};

}

#endif // CORE_STONE_H
